/**
 * Jackpot history routes
 * prefix: /jackpot-histories
 */

import express from 'express'
import middleware from '../system/middleware'
import controller from './controller'

const router = express.Router()

/**
 * @apiDefine Header
 * @apiHeader {string} Authorization Access token
 */

/**
 * @api {get} /jackpot-histories/:gameType Jackpot histories
 * @apiUse Header
 * @apiGroup Jackpot histories
 * @apiName Jackpot histories
 *
 * @apiDescription
 * gameType: slotTwelveDesignations, slotDragonBall
 *
 */
router.get('/:gameType', middleware.requiresLogin, controller.histories)

export default router

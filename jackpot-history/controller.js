import lodash from 'lodash'
import { response } from '../../utils'
import configs from '../../configs'
import { GameHistoryModel } from '../../packages/model'

/**
 * Show list jackpot histories
 */
const histories = async (req, res) => {
  const { page = 0 } = req.query
  const gameType = req.params
  const query = {
    ...gameType,
    ...lodash.pick(req.query, ['timestamp']),
    isWinJackpot: true,
  }
  const limit = configs.limit.gameHistories.all
  const data = await Promise.all([{
    histories: await GameHistoryModel.findByCondition(query, { page, limit }),
    total: await GameHistoryModel.countByCondition(query),
    limitPerPage: limit,
  }])
  const result = data[0]
  result.histories = await Promise.all(result.histories.map(async (item) => {
    const gameHistory = await GameHistoryModel.briefInfo(item)
    return gameHistory
  }))
  return response.r200(res, result)
}

export default {
  histories,
}

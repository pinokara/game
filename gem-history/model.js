import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const { ObjectId } = Schema.Types

const schema = new Schema({
  from: {
    type: ObjectId,
    ref: 'User',
  },
  to: {
    type: ObjectId,
    ref: 'User',
  },
  gem: {
    type: Number,
    min: [0],
  },
  fee: {
    type: Number,
    deault: 5,
  },
  note: {
    type: String,
    default: '',
  },
  exchangeRate: {
    type: Number,
    default: 0,
  },
  fromCurrentGem: {
    type: Number,
    default: 0,
  },
  toCurrentGem: {
    type: Number,
    default: 0,
  },
  isFromUserAgency: {
    type: Boolean,
    default: false,
  },
  isToUserAgency: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
}, {
  versionKey: false,
  collection: 'gem-histories',
})

schema.statics = statics

export default mongoose.model('GemHistory', schema)

import { to } from '../../utils'
import { GemHistoryModel } from '../model'

/**
 * Create new history
 *
 * @param {Object}  payload Data
 */
const newDoc = async (payload) => {
  // Save
  const obj = new GemHistoryModel(payload)
  const result = await to(obj.save())
  return result
}

/**
 * Get histories
 *
 * @param {ObjectId} userId
 * @param {Number} page
 * @param {Number} limit
 */
const getUserHistories = async (userId, page, limit) => {
  const condition = {
    $or: [{
      from: userId,
    }, {
      to: userId,
    }],
  }
  const { data } = await to(GemHistoryModel.find(condition).sort('-createdAt').skip(page * limit).limit(limit)
    .populate('from', 'username').populate('to', 'username').lean())

  return data
}

/**
 * Count total histories
 *
 * @param {ObjectId} userId
 */
const totalUserHistories = async (userId) => {
  const condition = {
    $or: [{
      from: userId,
    }, {
      to: userId,
    }],
  }
  const { data } = await to(GemHistoryModel.countDocuments(condition))
  return data
}

export default {
  newDoc,
  getUserHistories,
  totalUserHistories,
}

import config from '../../configs'
import { mongoose, Schema } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  // User
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },

  // Phone
  phone: {
    type: String,
    default: '',
  },

  // Code
  code: String,

  // Expire at time, default expire after 30m
  expireAt: {
    type: Date,
    default: Date.now() + config.verificationCode.defaultExpireTime,
  },

  // IP address
  remoteIP: {
    type: String,
    default: '',
  },

  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
  collection: 'verification-codes',
})

/**
 * Static
 */
schema.statics = statics

// Export
export default mongoose.model('VerificationCode', schema)

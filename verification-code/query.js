/**
 * Generate query for condition
 *
 * @param {Object}  condition
 */
const findByCondition = ({ remoteIP, phone, code, checkExpire, start, end }) => {
  const condition = {}

  if (remoteIP) {
    condition.remoteIP = remoteIP
  }

  if (phone) {
    condition.phone = phone
  }

  if (code) {
    condition.code = code
  }

  if (checkExpire) {
    condition.expireAt = {
      $gte: Date.now(),
    }
  }

  if (start && end) {
    condition.createdAt = {
      $gte: start,
      $lte: end,
    }
  }

  // Return
  return condition
}

export default {
  findByCondition,
}

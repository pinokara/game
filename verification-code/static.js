import config from '../../configs'
import { to, helper, time, getError, format } from '../../utils'
import { OTPModule } from '../../modules'
import { VerificationCodeModel } from '../model'
import dbQuery from './query'

/**
 * Check code valid or not
 *
 * @param {String} phone
 * @param {String} code
 */
const checkCodeValid = async (phone, code) => {
  const query = dbQuery.findByCondition({ phone, code, checkExpire: true })
  const { data } = await to(VerificationCodeModel.countDocuments(query))
  return data
}

/**
 * Count total sent SMS in time range
 *
 * @param {String} remoteIP remote IP
 * @param {String} phone phone number
 * @param {Date} start start of time range
 * @param {Date} end end of time range
 */
const totalSentOfTargetInTimeRange = async (remoteIP, phone, start, end) => {
  const query = dbQuery.findByCondition({ remoteIP, phone, start, end })
  const { data } = await to(VerificationCodeModel.countDocuments(query))
  return data
}

/**
 * Create new verification code request, send via SMS
 *
 * @param {Object} data request data
 * @param {String} locale locale
 */
const newRequestViaSMS = async (req) => {
  const remoteIP = helper.getRequestIP(req)

  // Get phone data
  req.body.phone = format.phone(req.body.phone)
  const { phone } = req.body

  const startOfToday = time.startOfToday()
  const endOfToday = time.endOfToday()

  // Check maximum sent
  const result = await Promise.all([{
    phone: await totalSentOfTargetInTimeRange('', phone, startOfToday, endOfToday),
    remoteIP: await totalSentOfTargetInTimeRange(remoteIP, '', startOfToday, endOfToday),
  }])

  // If over maximum quota, return error
  if (
    result[0].phone >= config.verificationCode.maxSentOfPhonePerDay ||
    result[0].remoteIP >= config.verificationCode.maxSentOfRemoteIPPerDay
  ) {
    return { error: true, data: config.message.reachMaxOTPPerDay }
  }

  // Create doc
  const { error, data } = await newDoc({
    phone,
    remoteIP,
  })

  if (error) {
    return { error: true, data: getError.message(error) }
  }

  return { error: false, data: data.toJSON() }
}

/**
 * Create new doc
 *
 * @param {Object} payload doc data
 * @param {String} locale for sms language
 */
const newDoc = async (payload) => {
  const code = helper.randomVerificationCode()
  const doc = new VerificationCodeModel({
    ...payload,
    code,
    expireAt: Date.now() + config.verificationCode.defaultExpireTime,
  })
  const result = await to(doc.save())

  // Send SMS to if not error
  if (!result.error) {
    const content = `Ma OTP cua quy khach la ${code}`
    OTPModule(payload.phone, content)
  }

  return result
}

export default {
  checkCodeValid,
  totalSentOfTargetInTimeRange,
  newRequestViaSMS,
  newDoc,
}

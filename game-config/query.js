
/**
 * Generate query from condition
 *
 * @param {Object} condition for generate query
 */
const findByCondition = ({ gameType }) => {
  const condition = {}

  if (gameType) {
    condition.gameType = gameType
  }
  return condition
}

export default {
  findByCondition,
}

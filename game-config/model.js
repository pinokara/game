import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  gameType: {
    type: String,
  },
  options: Schema.Types.Mixed,
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  collection: 'game-configs',
  versionKey: false,
})

schema.statics = statics

export default mongoose.model('GameConfig', schema)

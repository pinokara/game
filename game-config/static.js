import { GameConfigModel } from '../model'
import dbQuery from './query'

/**
 * Find one by condition
 *
 * @param {Object} condition
 */
const findOneByCondition = async (condition) => {
  const query = dbQuery.findByCondition(condition)
  const data = await GameConfigModel.findOne(query).lean()
  return data
}

export default {
  findOneByCondition,
}

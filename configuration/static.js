import { to } from '../../utils'
import { ConfigModel } from '../model'

/**
 * Get app config
 */
const getConfig = async () => {
  const { data } = await to(ConfigModel.findOne().lean())
  if (data) return data

  const doc = new ConfigModel()
  await doc.save()
  return doc.toJSON()
}

/**
 * Set maintain server status to global variable
 */
const setMaintainServerKey = async () => {
  const doc = await getConfig()
  global.isMaintainingServer = doc.isMaintainingServer
}

export default {
  getConfig,
  setMaintainServerKey,
}

import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  app: {
    android: {
      type: String,
      default: '1.0',
    },
    ios: {
      type: String,
      default: '1.0',
    },
  },
  exchangeCardRate: {
    type: Number,
    default: 1.2,
  },
  submitCardRate: {
    type: Number,
    default: 0.8,
  },
  isMaintainingServer: {
    type: Boolean,
    default: false,
  },
  registerGem: {
    type: Number,
    default: 0,
  },
  allowLoginWebCard: [{
    type: Schema.Types.ObjectId,
    ref: 'User',
  }],
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  collection: 'configurations',
  versionKey: false,
})

schema.statics = statics

export default mongoose.model('Config', schema)

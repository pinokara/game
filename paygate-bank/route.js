/**
 * Game routes
 * prefix: /games
 */

import express from 'express'
import middleware from '../system/middleware'
import PayGateBankCtl from './controller'
import validation from './validation'

const router = express.Router()


router.post('/transaction', middleware.requiresLogin, validation.bank, PayGateBankCtl.transaction)

export default router

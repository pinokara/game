// import lodash from 'lodash'
import JWT from 'jsonwebtoken'
import axios from 'axios'
import crypto from 'crypto'


import config from '../../configs'
import { response } from '../../utils'
import { UserModel, BankTransactionModel } from '../../packages/model'

const transaction = async (req, res) => {
  const user = req.user;
  if (!user || !user._id) return response.r404(res, config.message.userNotFound)

  // save transaction
  const doc = new BankTransactionModel({
    command: req.body.command,
    bank_account_id: req.body.bank_account_id,
    amount: req.body.amount,
    account_name: req.body.account_name,
    transaction_code: req.body.transaction_code,
    slip_type: req.body.slip_type,
    user_id: user._id,
    username: user.username
  });

  await doc.save();
  return response.r200(res)
}


export default {
  transaction,
}

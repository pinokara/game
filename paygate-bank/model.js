import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  account_name: String,
  username: String,
  amount: Number,
  bank_acccount_id: String,
  command: String,
  member_id: String,
  slip_type: Number,
  status: {
    type: Number,
    default: 0,
  },
  transaction_code: String,
  user_id: String,
  note: {
    type: String,
    default: '',
  },
  is_view: {
    type: Number,
    default: 0,
  },
  chargedAt: Date,
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

schema.index({ sendBy: 1 }).index({ createdAt: 1 }).index({ isCharged: 1 })

schema.statics = statics

export default mongoose.model('BankTransaction', schema)

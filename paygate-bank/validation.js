import lodash from 'lodash'
import Joi from 'joi'
import { validateClientData } from '../../utils'
import configs from '../../configs'

const { message } = configs

const obj = {


  amount: Joi.number().required().options({
    language: {
      key: 'Số tiền ',
      number: {
        base: `không hợp lệ`,
      },
      any: {
        required: `không được trống`,
        empty: `không được trống`,
        allowOnly: `không hợp lệ`,
      },
    },
  }),

  transaction_code: Joi.string().required().options({
    language: {
      key: 'Mã giao dich ',
      any: {
        required: `không được trống`,
        empty: `không được trống`,
        allowOnly: `không hỗ trợ`,
      },
    }
  }),

  bank_name: Joi.string().valid(['acb', 'vietcombank']).required().options({
    language: {
      key: 'Ngân hàng ',
      string: {
        base: 'không hỗ trợ',
      },
      any: {
        required: `không được trống`,
        empty: `không được trống`,
        allowOnly: `không hỗ trợ`,
      },
    },
  }),

  account_name: Joi.string().required().options({
    language: {
      key: 'Tên tài khoản ',
      any: {
        required: `không được trống`,
        empty: `không được trống`,
        allowOnly: `không hợp lệ`,
      },
    },
  }),

  account_holder: Joi.string().required().options({
    language: {
      key: 'Tên chủ thẻ ',
      any: {
        required: `không được trống`,
        empty: `không được trống`,
        allowOnly: `không hợp lệ`,
      },
    },
  }),
}

const bank = (req, res, next) => {
  validateClientData(req, res, next, Joi.object().keys(lodash.pick(obj, ['amount', 'bank_name', 'account_name', 'account_holder', 'transaction_code'])))
}

export default {
  bank,
}

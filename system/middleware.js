/**
 * API middleware
 */
import configs from '../../configs'
import { response } from '../../utils'

/**
 * ****************************************
 * CHECK ROLE
 * ****************************************
 */

/**
 * Check valid token
 */
const isAuthenticated = async (req) => {
  return req.user && req.user._id
}

/**
 * ****************************************
 * REQUIRE
 * ****************************************
 */

/**
 * Require user logged in to do next action
 *
 */
const requiresLogin = async (req, res, next) => {
  const isAuthorized = await isAuthenticated(req)
  if (!isAuthorized) {
    return response.r401(res, configs.message.noPermission)
  }
  next()
}

// Export
export default {
  requiresLogin,
}

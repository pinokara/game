import lodash from 'lodash'
import Joi from 'joi'
import { validateClientData } from '../../utils'
import configs from '../../configs'

const { message } = configs

const obj = {
  username: Joi.string().required().options({
    language: {
      key: '{{!username}}',
      string: {
        base: `!!${message.usernameMustBeAString}`,
      },
      any: {
        required: `!!${message.usernameRequired}`,
        empty: `!!${message.usernameRequired}`,
      },
    },
  }),
  password: Joi.string().required().min(6).options({
    language: {
      key: '{{!username}}',
      string: {
        base: `!!${message.passwordMustBeAString}`,
        min: `!!${message.passwordMustLeastAt6}`,
      },
      any: {
        required: `!!${message.passwordRequired}`,
        empty: `!!${message.passwordRequired}`,
      },
    },
  }),
  phone: Joi.string().regex(configs.regex.phone).required().options({
    language: {
      key: '{{!phone}}',
      string: {
        base: `!!${message.phoneIsRequired}`,
        regex: {
          base: `!!${message.invalidPhone}`,
        },
      },
      any: {
        required: `!!${message.phoneIsRequired}`,
        empty: `!!${message.phoneIsRequired}`,
      },
    },
  }),
  access_token: Joi.string().required().options({
    language: {
      key: '{{!access_token}}',
      string: {
        base: `!!${message.AccessTokenIsRequired}`,
      },
      any: {
        required: `!!${message.AccessTokenIsRequired}`,
        empty: `!!${message.AccessTokenIsRequired}`,
      },
    },
  }),
}

const login = (req, res, next) => {
  validateClientData(req, res, next, Joi.object().keys(lodash.pick(obj, ['password'])))
}

const register = (req, res, next) => {
  validateClientData(req, res, next, Joi.object().keys(lodash.pick(obj, ['username', 'password'])))
}

const registerWithPhone = (req, res, next) => {
  validateClientData(req, res, next, Joi.object().keys(lodash.pick(obj, ['phone', 'username', 'password'])))
}

const requestVerificationCode = (req, res, next) => {
  validateClientData(req, res, next, Joi.object().keys(lodash.pick(obj, ['phone'])))
}
const loginFB = (req, res, next) => {
  validateClientData(req, res, next, Joi.object().keys(lodash.pick(obj, ['access_token'])))
}

export default {
  login,
  register,
  registerWithPhone,
  requestVerificationCode,
  loginFB
}

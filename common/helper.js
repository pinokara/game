import lodash from 'lodash'
import gameplayConfig from '../../configs/gameplay'
import { helper } from '../../utils'

const getAppData = async () => {
  const gameConfig = gameplayConfig.get()
  let games = gameConfig.hasJackpotGames._.split(' ')
  games = await Promise.all(games.map(async (gameId) => {
    const game = gameConfig[gameId]

    // Get betValues from config
    const betValues = await Promise.all(game.betValues.map(async (item) => {
      const currentJackpotValue = await helper.getJackpotValue(`${gameId}_${item.value}`)
      // console.log(gameId +" currentJackpotValue "+currentJackpotValue)
      return {
        ...item,
        currentJackpotValue,
      }
    }))
    return {
      ...lodash.pick(game, ['gameId', 'jackpotId', 'wildId', 'items', 'payLines']),
      betValues,
    }
  }))
  return games
}

export default {
  getAppData,
}

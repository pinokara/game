import lodash from 'lodash'
import gameplayConfig from '../../configs/gameplay'
import { response, token, getError, format, to } from '../../utils'
import { UserModel, ConfigModel, VerificationCodeModel } from '../../packages/model'
import configs from '../../configs'
import helper from './helper'
import { TelegramModule } from '../../modules'
import Pusher from 'pusher'
import { now } from 'moment'

const cryptoRandomString = require('crypto-random-string');

var FacebookTokenStrategy = require('passport-facebook-token');
var passport = require('passport')

const { message } = configs


const pusher = new Pusher({
    appId: process.env.PUSHER_APP_ID,
    key: process.env.PUSHER_APP_KEY,
    secret: process.env.PUSHER_APP_SECRET,
    cluster: process.env.PUSHER_APP_CLUSTER,
});

/**
 * Ping
 */
const ping = (req, res) => {
    return response.r200(res, { pong: true })
}

/**
 * Login
 *
 */
const login = async (req, res) => {
    const { username, password } = req.body

    if (!username || !password) {
        console.log("LOGIN", username, password)
        return response.r400(res, message.usernameOrPassIncorrect)
    }

    const { data } = await to(UserModel.findOne({ username: username, isBlock: false }))
    if (!data) {
        console.log("LOGIN", username, password, "DATA", data)
        return response.r400(res, message.usernameOrPassIncorrect)
    }

    // Check password
    const isAuthenticated = data.authenticate(password)
    if (!isAuthenticated) {
        console.log("LOGIN", username, password, "WRONG")
        return response.r400(res, message.usernameOrPassIncorrect)
    }
    // push socket id
    console.log('Socket ID', data.socket_id);
    if (data.socket_id) {
        pusher.trigger('notifications', 'forceDisconnect', data.socket_id);
    }
    const jsonData = lodash.pick(data.toJSON(), ['_id', 'username'])
    jsonData.time = now()
    var accessToken = cryptoRandomString({ length: 256, type: 'base64' })
    jsonData.accessToken = accessToken
    await UserModel.updateDocById(data._id, { accessToken: accessToken })
    var jwtToken = token(jsonData)
    delete jsonData.time
    delete jsonData.accessToken
    response.r200(res, {
        token: jwtToken,
        user: jsonData,
    })
}
/**
 * Login fb
 */
const loginFB = async (req, res) => {
    passport.use(new FacebookTokenStrategy({
        clientID: process.env.FB_ID,
        clientSecret: process.env.FB_SECRET
    }, function (accessToken, refreshToken, profile, done) {
        // console.log('Profile', profile);
        UserModel.upsertFbUser(accessToken, refreshToken, profile, function (err, user) {
            if (err) {
                return response.r400(res, 'Có lỗi trong quá trình thực hiện')
            }
            return done(err, user);
        });
    }
    ));
    passport.authenticate('facebook-token', async function (error, user, info) {
        if (!user) {
            return response.r400(res, 'Có lỗi trong quá trình thực hiện')
        }
        // push socket id
        if (user.socket_id) {
            pusher.trigger('notifications', 'forceDisconnect', user.socket_id);
        }
        // const jsonData = lodash.pick(user.toJSON(), ['_id', 'username'])
        // return response.r200(res, {
        //     token: token(jsonData),
        //     user: jsonData,
        // })
        const jsonData = lodash.pick(user.toJSON(), ['_id', 'username'])
        jsonData.time = now()
        var accessToken = cryptoRandomString({ length: 256, type: 'base64' })
        jsonData.accessToken = accessToken
        await UserModel.updateDocById(data._id, { accessToken: accessToken })
        var jwtToken = token(jsonData)
        delete jsonData.time
        delete jsonData.accessToken
        response.r200(res, {
            token: jwtToken,
            user: jsonData,
        })
    })(req, res);
}

/**
 * Login web card
 *
 */
const loginWebCard = async (req, res) => {
    const { username, password } = req.body

    if (!username || !password) {
        return response.r400(res, message.usernameOrPassIncorrect)
    }

    const { data } = await to(UserModel.findOne({ username }))
    if (!data) {
        return response.r400(res, message.usernameOrPassIncorrect)
    }

    // Check password
    const isAuthenticated = data.authenticate(password)
    if (!isAuthenticated) {
        return response.r400(res, message.usernameOrPassIncorrect)
    }

    const cfg = await ConfigModel.getConfig()
    const index = cfg.allowLoginWebCard.findIndex(item => item.toString() === data._id.toString())
    if (index === -1) {
        return response.r400(res, message.noPermission)
    }

    const jsonData = lodash.pick(data.toJSON(), ['_id', 'username'])
    response.r200(res, {
        token: token(jsonData),
        user: jsonData,
    })
}

/**
 * Login
 *
 */
const loginWithPhone = async (req, res) => {
    const { code } = req.body
    const phone = format.phone(req.body.phone)

    if (!phone) {
        return response.r400(res, message.usernameOrPassIncorrect)
    }

    // Check verification code
    const isVerificationCodeValid = await VerificationCodeModel.checkCodeValid(phone, code)
    if (!isVerificationCodeValid) {
        return response.r400(res, message.invalidOTPCode)
    }

    const { data } = await to(UserModel.findOne({ phone, verified: true }))
    if (!data) {
        return response.r400(res, message.usernameOrPassIncorrect)
    }

    const jsonData = lodash.pick(data.toJSON(), ['_id', 'username'])
    response.r200(res, {
        token: token(jsonData),
        user: jsonData,
    })
}

/**
 * Register
 */
const register = async (req, res) => {
    const cfgDoc = await ConfigModel.getConfig()
    req.body.gem = cfgDoc.registerGem || 0

    // Check username exists
    const isUsernameExisted = await UserModel.checkUsernameExisted(req.body.username)

    if (isUsernameExisted) {
        return response.r400(res, message.usernameExisted)
    }
    var userInfo = req.body
    // userInfo.gem = 1000000

    try {
        const data = await UserModel.newDoc(userInfo)
        console.log(data)
        if (!data) {
            return response.r400(res, getError.message(error))
        }

        const jsonData = lodash.pick(data.toJSON(), ['_id', 'username'])
        jsonData.time = now()
        var accessToken = cryptoRandomString({ length: 256, type: 'base64' })
        jsonData.accessToken = accessToken
        await UserModel.updateDocById(data._id, { accessToken: accessToken })
        var jwtToken = token(jsonData)
        delete jsonData.time
        delete jsonData.accessToken
        response.r200(res, {
            token: jwtToken,
            user: jsonData,
        })
    } catch (err) {
        console.log(err)
        return response.r400(res, getError.message(err))
    }

}

/**
 * Register with phone
 */
const registerWithPhone = async (req, res) => {
    const cfgDoc = await ConfigModel.getConfig()
    req.body.gem = cfgDoc.registerGem || 0
    req.body.phone = format.phone(req.body.phone)
    const { phone, username } = req.body
    // const { phone, code, username } = req.body

    // Check verification code
    // const isVerificationCodeValid = await VerificationCodeModel.checkCodeValid(phone, code)
    // if (!isVerificationCodeValid) {
    //   return response.r400(res, message.invalidOTPCode)
    // }

    // Check username exists
    const isUsernameExisted = await UserModel.checkUsernameExisted(username)
    if (isUsernameExisted) {
        return response.r400(res, message.usernameExisted)
    }

    // Check phone exists
    const isPhoneExisted = await UserModel.checkPhoneExisted(phone)
    if (isPhoneExisted) {
        return response.r400(res, message.phoneExisted)
    }

    req.body.verified = true
    const { error } = await UserModel.newDoc(req.body)
    if (error) {
        console.log('error', error)
        return response.r400(res, getError.message(error))
    }
    return response.r200(res, {})
}

/**
 * Request verification code
 */
const requestVerificationCode = async (req, res) => {
    const { error } = await VerificationCodeModel.newRequestViaSMS(req)
    if (error) {
        return response.r400(res, getError.message(error))
    }
    return response.r200(res, {}, configs.message.OTPSent)
}

/**
 * Check phone number existed or not
 */
const checkPhoneExist = async (req, res) => {
    const existed = await UserModel.checkPhoneExisted(req.query.phone)
    return response.r200(res, { existed })
}

/**
 * Get app data
 *
 */
const getAppData = async (req, res) => {
    const games = await helper.getAppData()
    const cfg = gameplayConfig.get()
    return response.r200(res, {
        games,
        sendGem: cfg.agency,
    })
}

/**
 * Get list agencies
 *
 */
const agencies = async (req, res) => {
    const results = await UserModel.getListAgencies()
    return response.r200(res, { agencies: results })
}

const requestForgotPassword = async (req, res) => {
    const { username } = req.body
    if (!username) {
        return response.r400(res, message.usernameRequired)
    }

    const { data } = await to(UserModel.findOne({ username }))
    if (!data) {
        return response.r400(res, message.dataNotFound)
    }

    if (!data.isTelegram) {
        return response.r400(res, message.accountNotLinkTelegram)
    }

    TelegramModule.sendOTP(data._id)

    return response.r200(res)
}

const verifyOTPForgotPassword = async (req, res) => {
    const { username, code, newPass } = req.body
    if (!username) {
        return response.r400(res, message.usernameRequired)
    }

    if (!code) {
        return response.r400(res, message.invalidOTPCode)
    }

    if (!newPass) {
        return response.r400(res, message.passwordRequired)
    }

    const { data } = await to(UserModel.findOne({ username }))
    if (!data) {
        return response.r400(res, message.dataNotFound)
    }

    if (!data.isTelegram) {
        return response.r400(res, message.accountNotLinkTelegram)
    }

    const result = await TelegramModule.verifyOTP(data._id, code)
    if (!result) {
        data.password = newPass
        await data.save()
        return response.r200(res, {}, message.passwordChanged)
    } else {
        return response.r400(res, result)
    }
}

const getCurrentVersion = (req, res) =>{
    return response.r200(res, configs.clientVersion)
} 

export default {
    ping,
    login,
    loginWebCard,
    loginWithPhone,
    register,
    registerWithPhone,
    requestVerificationCode,
    getAppData,
    agencies,
    checkPhoneExist,
    requestForgotPassword,
    verifyOTPForgotPassword,
    loginFB,
    getCurrentVersion,
}

/**
 * Common routes
 * prefix: /
 */

import express from 'express'
import middleware from '../system/middleware'
import CommonCtrl from './controller'
import validation from './validation'

const router = express.Router()

/**
 * @api {get} /ping Ping
 * @apiGroup Common
 * @apiName Ping
 */
router.get('/ping', CommonCtrl.ping)

/**
 * @api {post} /register Register new user
 * @apiGroup Common
 * @apiName Register
 *
 * @apiParam {String} username
 * @apiParam {String} password
 * @apiParam {String} phone
 */
router.post('/register', validation.register, CommonCtrl.register)

/**
 * @api {post} /register-with-phone Register with phone number
 * @apiGroup Common
 * @apiName Register with phone number
 *
 * @apiParam {String} username
 * @apiParam {String} password
 * @apiParam {String} phone
 */
router.post('/register-with-phone', validation.registerWithPhone, CommonCtrl.registerWithPhone)

/**
 * @api {post} /request-verification-code Request verification code
 * @apiGroup Common
 * @apiName Request verification code
 *
 * @apiParam {String} phone
 */
router.post('/request-verification-code', validation.requestVerificationCode, CommonCtrl.requestVerificationCode)

/**
 * @api {get} /check-phone-exists Check phone exists
 * @apiGroup Common
 * @apiName Check phone exists
 *
 * @apiParam {String} phone
 */
router.get('/check-phone-exists', validation.requestVerificationCode, CommonCtrl.checkPhoneExist)

/**
 * @api {post} /login Login
 * @apiGroup Common
 * @apiName Login
 *
 * @apiParam {String} username
 * @apiParam {String} password
 */
router.post('/login', validation.login, CommonCtrl.login)


/**
 * @api {post} /login-fb Login Fb
 * @apiGroup Common
 * @apiName Login
 *
 * @apiParam {String} token
 */
router.post('/login-fb', validation.loginFB, CommonCtrl.loginFB)

/**
 * @api {post} /login-web-card Login web card
 * @apiGroup Common
 * @apiName Login web card
 *
 * @apiParam {String} username
 * @apiParam {String} password
 */
router.post('/login-web-card', validation.login, CommonCtrl.loginWebCard)

/**
 * @api {post} /login Login with phone number
 * @apiGroup Common
 * @apiName LoginWithPhoneNumber
 *
 * @apiParam {String} phone
 * @apiParam {String} code
 */
router.post('/login-with-phone', CommonCtrl.loginWithPhone)

/**
 * @api {get} /app-data App data
 * @apiGroup Common
 * @apiName App data
 */
router.get('/app-data', CommonCtrl.getAppData)

/**
 * @api {get} /agencies Agencies
 * @apiGroup Common
 * @apiName Agencies
 */
router.get('/agencies', middleware.requiresLogin, CommonCtrl.agencies)

/**
 * @api {post} /requestForgotPassword
 * @apiGroup Common
 * @apiName requestForgotPassword
 */
router.post('/requestForgotPassword', CommonCtrl.requestForgotPassword)

/**
 * @api {post} /verifyOTPForgotPassword
 * @apiGroup Common
 * @apiName verifyOTPForgotPassword
 */
router.post('/verifyOTPForgotPassword', CommonCtrl.verifyOTPForgotPassword)

/**
 * @api {get} /getVersion
 * @apiGroup Common
 * @apiName get current client version
 */
router.get('/getVersion', CommonCtrl.getCurrentVersion)

export default router

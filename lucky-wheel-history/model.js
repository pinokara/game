import { mongoose, Schema } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
  },
  prize: {},
  restGem: {
    type: Number,
    default: 0,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

// Index
schema.index({ user: 1 }).index({ user: 1, createdAt: 1 })

/**
 * Static functions
 *
 */
schema.statics = statics

// Export
export default mongoose.model('LuckyWheelHistory', schema)

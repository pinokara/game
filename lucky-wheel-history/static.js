import lodash from 'lodash'
import { to } from '../../utils'
import { LuckyWheelHistoryModel } from '../model'
import dbQuery from './query'

/**
 * Find by condition
 *
 * @param {Object} condition condition
 * @param {Object} pagination
 */
const findByCondition = async (condition, { page, limit }) => {
  const query = dbQuery.findByCondition(condition)
  const { data } = await to(LuckyWheelHistoryModel.find(query).sort('-createdAt').populate('user', 'username verified').skip(page * limit).limit(limit).lean())

  if (!data || !data.length) {
    return []
  }

  // Filter return data
  const result = data.map((item) => {
    const obj = info(item)
    return obj
  })
  return result
}

/**
 * Get user spin histories
 *
 * @param {ObjectId}  userId user id
 * @param {Object}    pagination
 */
const getByUser = async (userId, { page, limit }) => {
  const { data } = await to(LuckyWheelHistoryModel.find({
    user: userId,
  }).sort('-createdAt').skip(page * limit).limit(limit).lean())

  if (!data || !data.length) {
    return []
  }

  // Filter return data
  const result = data.map((item) => {
    const obj = info(item)
    return obj
  })
  return result
}

/**
 * Get info
 *
 */
const info = (data) => {
  const obj = lodash.pick(data, ['_id', 'createdAt', 'user'])
  obj.prize = lodash.pick(data.prize, ['order', 'type', 'gem', 'spin'])
  return obj
}

export default {
  findByCondition,
  getByUser,
}

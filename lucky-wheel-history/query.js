import { ObjectId } from '../../utils/mongoose'
import { validation } from '../../utils'

/**
 * Find by condition
 *
 * @param {Object} condition
 */
const findByCondition = ({ user, startAt, endAt }) => {
  const condition = {}

  if (user && validation.isObjectId(user)) {
    condition.user = new ObjectId(user)
  }

  if (startAt && endAt) {
    condition.createdAt = {
      $gte: startAt,
      $lte: endAt,
    }
  }

  return condition
}

export default {
  findByCondition,
}

import lodash from 'lodash'
import { response } from '../../utils'
import configs from '../../configs'
import { GameHistoryModel } from '../../packages/model'
import moment from 'moment'

/**
 * Show list game histories
 */
const histories = async (req, res) => {
  const { page = 0 } = req.query
  const user = req.user
  const gameType = req.params
  const query = {
    ...gameType,
    ...lodash.pick(req.query, ['timestamp']),
    user: user._id
  }
  console.log(query)
  const limit = configs.limit.gameHistories.all
  const data = await Promise.all([{
    histories: await GameHistoryModel.findByCondition(query, { page, limit }),
    total: await GameHistoryModel.countByCondition(query),
    limitPerPage: limit,
  }])
  const result = data[0]
  result.histories = await Promise.all(result.histories.map(async (item) => {
    const gameHistory = await GameHistoryModel.briefInfo(item)
    gameHistory.createdAtMs = moment(gameHistory.createdAt).unix()
    // console.log("gameHistory: ",gameHistory)
    return gameHistory
  }))
  return response.r200(res, result)
}

export default {
  histories,
}

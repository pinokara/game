/**
 * Game history routes
 * prefix: /game-histories
 */

import express from 'express'
import middleware from '../system/middleware'
import controller from './controller'

const router = express.Router()

/**
 * @apiDefine Header
 * @apiHeader {string} Authorization Access token
 */

/**
 * @api {get} /game-histories/:gameType Game histories
 * @apiUse Header
 * @apiGroup Game histories
 * @apiName Game histories
 *
 * @apiDescription
 * gameType: slotTwelveDesignations, slotDragonBall
 */
router.get('/:gameType', middleware.requiresLogin, controller.histories)

export default router

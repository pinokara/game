import { validation } from '../../utils'
import { ObjectId } from '../../utils/mongoose'
// import configs from '../../configs'

/**
 * Find condition from object
 *
 * @param {Object} condition allow _id properties
 */
const findByCondition = ({ _id, gameType, timestamp, isWinJackpot, user }) => {
  const condition = {}
  // _id
  if (_id && validation.isObjectId(_id)) {
    condition._id = new ObjectId(_id)
  }
  if (gameType) {
    condition.gameType = gameType
  }
  if (timestamp) {
    condition.createdAt = {
      $lt: new Date(timestamp),
    }
  }
  if (validation.isBoolean(isWinJackpot)) {
    condition.isWinJackpot = isWinJackpot
  }
  if (user) {
    condition.user = new ObjectId(user)
  }
  return condition
}

export default {
  findByCondition,
}

import lodash from 'lodash'
import moment from 'moment'
import { GameHistoryModel, UserModel } from '../model'
import { to } from '../../utils'
import dbQuery from './query'
import helper from '../../utils/helper'

const _ = require('lodash');

/**
 * Find docs by condition
 *
 * @param {Object} condition query condition
 * @param {Object} pagination pagination data
 * @param {String} sort sort
 */
const findByCondition = async (condition, { page = 0, limit }, sort = '-createdAt') => {
  const query = dbQuery.findByCondition(condition)
  const { data } = await to(GameHistoryModel.find(query).sort(sort).skip(page * limit).limit(limit))
  return data || []
}

/**
 * Count by condition
 *
 * @param {Object} condition find condition
 */
const countByCondition = async (condition) => {
  const query = dbQuery.findByCondition(condition)
  const { data } = await to(GameHistoryModel.countDocuments(query))
  return data
}

/**
 * Brief info game hitory
 *
 * @param {Object} gameHistory data game history
 */
const briefInfo = async (gameHistory) => {
  const data = await Promise.all([{
    user: await UserModel.getById(gameHistory.user),
  }])
  const result = data[0]
  result.user = lodash.pick(result.user, ['_id', 'username'])
  return {
    ...lodash.pick(gameHistory, ['_id', 'winGem', 'isWinJackpot', 'betValue', 'createdAt', 'refundGem']),
    ...result,
  }
}

/**
 * Get list histories by game id
 *
 * @param {String} gameId
 */
const getByGameId = async (gameType, gameId) => {
  const now = new Date()
  const daysAgo = new Date(moment().subtract(10, 'd').toISOString())

  const { data } = await to(GameHistoryModel.find({
    gameType,
    gameId,
    createdAt: {
      $gte: daysAgo,
      $lte: now,
    },
  }).sort('-createdAt').select('winGem user options createdAt betValue isWin isWinJackpot refundGem').populate('user', 'username').lean())

  if (!data || !data.length) {
    return []
  }

  return data
}

/**
 * Get list histories by user id
 *
 * @param {String} gameId
 */
const getByUserId = async (userId, gameType) => {
  const now = new Date()
  const daysAgo = new Date(moment().subtract(6, 'months').toISOString())

  const { data } = await to(GameHistoryModel.find({
    gameType,
    user: userId,
    createdAt: {
      $gte: daysAgo,
      $lte: now,
    },
  }).sort('-createdAt').select('gameId winGem user options createdAt betValue isWin isWinJackpot refundGem').populate('user', 'username').lean())

  if (!data || !data.length) {
    return []
  }

  return data
}

/**
 * Get list by gameType
 *
 * @param {String} gameType
 */
const bigWin = async (gameType) => {
  // const start = new Date(moment().startOf('d').toISOString())
  // const end = new Date(moment().endOf('d').toISOString())
  // console.log("start ", start, " end ", end, "gameType", gameType)

  // const { data } = await to(GameHistoryModel.aggregate([{
  //   $match: {
  //     createdAt: {
  //       $gte: start,
  //       $lte: end,
  //     },
  //   },
  // }, {
  //   $match: {
  //     gameType,
  //   },
  // }, {
  //   $group: {
  //     _id: '$user',
  //     totalWin: { $sum: { $max: [0, { $add: ["$winGem", { $subtract: ["$refundGem", "$betValue"] }] }] } },
  //     totalBet: { $sum: '$betValue' },
  //   },
  // }, {
  //   $lookup: {
  //     from: 'users',
  //     localField: '_id',
  //     foreignField: '_id',
  //     as: 'user'
  //   }
  // }, {
  //   $match: { 'user.isBot': false }
  // }, {
  //   $sort: { totalWin: -1 },
  // }, {
  //   $limit: 20,
  // }]))

  // if (!data || !data.length) {
  //   return []
  // }

  // const result = Promise.all(data.map(async (item) => {
  //   // const user = await UserModel.getById(item._id)
  //   // if(user.isbot){
  //   //   return null
  //   // }
  //   return {
  //     _id: item._id,
  //     user: _.pick(item.user[0], ['_id', 'username', 'gem', 'verified']),
  //     totalWin: item.totalWin,
  //     totalBet: item.totalBet,
  //   }
  // }))

  const data = await helper.getListGameHonor(gameType, 0, 10);

  const chunkData = _.chunk(data, 2);
  const result = Promise.all(chunkData.map(async (item) => {
    const score = parseInt(item[1]);
    if (score == 0) {
      return null;
    }
    const user = await UserModel.getById(item[0])
    if (user.isbot) {
      return null
    }

    console.log('DATA BIGWIN: ', item);
    return {
      _id: item[0],
      user: _.pick(user, ['_id', 'username', 'gem', 'verified']),
      totalWin: item[1],
      totalBet: item[1],
    }
  }))
  console.log('BIG WIN: ', JSON.stringify(result));
  return result
}

/**
 * Get list by gameType
 *
 * @param {String} gameType
 */
const rankingWinLose = async (gameType) => {
  // const start = new Date()
  // const end = new Date(moment().endOf('d').toISOString())

  const { data } = await to(GameHistoryModel.aggregate([
    {
      $match: {
        gameType,
      },
    }, {
      $project: {
        user: 1,
        win: {
          $cond: [{ $eq: ['$isWin', true] }, 2, 0],
        },
        lose: {
          $cond: [{ $eq: ['$isWin', false] }, 1, 0],
        },
        totalWin: { $sum: '$winGem' },
        totalBet: { $sum: '$betValue' },
      },
    }, {
      $group: {
        _id: '$user',
        countLose: { $sum: '$lose' },
        countWin: { $sum: '$win' },
        totalWin: { $first: '$totalWin' },
        totalBet: { $first: '$totalBet' },
      },
    }, {
      $addFields: {
        result: { $subtract: ['$countWin', '$countLose'] },
      },
    }, {
      $lookup: {
        from: 'users',
        localField: '_id',
        foreignField: '_id',
        as: 'user',
      },
    }, {
      $match: { 'user.isBot': false },
    }, {
      $sort: { result: -1 },
    }, {
      $limit: 10,
    }]))

  if (!data || !data.length) {
    return []
  }

  const result = Promise.all(data.map(async (item) => {
    // const user = await UserModel.getById(item._id)
    return {
      _id: item._id,
      user: _.pick(item.user[0], ['_id', 'username', 'gem', 'verified']),
      countLose: item.countLose,
      countWin: item.countWin,
      totalWin: item.totalWin,
      totalBet: item.totalBet,
    }
  }))

  return result
}

export default {
  findByCondition,
  countByCondition,
  briefInfo,
  getByGameId,
  getByUserId,
  bigWin,
  rankingWinLose,
}

import config from '../../configs'
import { helper, format } from '../../utils'
import { UserModel, GameHistoryModel } from '../model'

const SOCKET_WIN_JACKPOT_KEY = 'global-win-jackpot'
const SOCKET_WIN_BIG_KEY = 'global-big-win'

const postSave = async (doc) => {
  Promise.all([
    UserModel.updateGameStats(doc),
  ])

  // Emit global if win gem >= 500k or win jackpot
  if (doc.winGem >= 500000 || doc.isWinJackpot) {
    const user = await UserModel.getById(doc.user)
    const gameName = helper.getGameName(doc.gameType)

    if (doc.isWinJackpot) {
      const message = config.message.winJackpot(user.username, gameName, format.number(doc.winGem))
      global.io.emit(SOCKET_WIN_JACKPOT_KEY, message)
    } else {
      const message = config.message.bigWin(user.username, gameName, format.number(doc.winGem))
      global.io.emit(SOCKET_WIN_BIG_KEY, message)

      // Update doc big win
      GameHistoryModel.updateOne({ _id: doc._id }, { $set: { isBigWin: true } }).exec()
    }
  }
}

export default {
  postSave,
}

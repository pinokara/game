import { mongoose, Schema } from '../../utils/mongoose'
import statics from './static'
import hooks from './hook'

const schema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
  },
  gameType: String,
  gameId: String,
  options: Schema.Types.Mixed,
  isWin: {
    type: Boolean,
    default: false,
  },
  isWinJackpot: {
    type: Boolean,
    default: false,
  },
  isBigWin: {
    type: Boolean,
    default: false,
  },
  winGem: {
    type: Number,
    default: 0,
  },
  refundGem: {
    type: Number,
    default: 0,
  },
  vpoint: {
    type: Number,
    default: 0,
  },
  restGem: {
    type: Number,
    default: 0,
  },
  betValue: {
    type: Number,
    default: 0,
  },
  bonus: {
    type: Number,
    default: 1,
  },
  bonusJackpot: {
    type: Number,
    default: 1,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

// Index
schema.index({ user: 1 }).index({ gameType: 1 }).index({ gameId: 1 }).index({ isWin: 1 }).index({ isWinJackpot: 1 })
  .index({ createdAt: 1 }).index({ createdAt: 1, winGem: 1 }).index({ isBigWin: 1 })

/**
 * Static functions
 *
 */
schema.statics = statics

/**
 * Pre save
 *
 */
schema.pre('save', function (next) {
  this.winGem = Math.round(this.winGem)
  next()
})

/**
 * Post findOneAndUpdate
 *
 */
schema.post('save', hooks.postSave)

// Export
export default mongoose.model('GameHistory', schema)

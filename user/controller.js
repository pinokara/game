import moment from 'moment'
import currencyFormater from 'currency-formatter'
import config from '../../configs'
import gameplayConfig from '../../configs/gameplay'
import { response, getError } from '../../utils'
import { UserModel, GemHistoryModel } from '../../packages/model'
import { TelegramModule } from '../../modules'


// const { message } = config

/**
 * Send gem to another user
 *
 */
const sendGem = async (req, res) => {
  const cfg = gameplayConfig.get()
  const { note, code } = req.body
  // const { note, code } = req.body
  const gem = parseFloat(req.body.gem)
  const targetUser = req.userData.toJSON()
  const targetUserId = targetUser._id
  const currentUserId = req.user._id
  const currentUser = await UserModel.findOneByCondition({ _id: currentUserId })
  const isNormalUser = !currentUser.role
  const isAdmin = currentUser.role === config.user.roles.admin
  const isAgency = config.user.agencies._.split(' ').includes(currentUser.agency)
  const isTargetAgency = config.user.agencies._.split(' ').includes(targetUser.agency)
  const { agency } = currentUser
  const targetAgency = targetUser.agency

  const isBannedSell = currentUser.bannedTransferGem.includes('sell')
  const isBannedBuy = targetUser.bannedTransferGem.includes('buy')


  if (!currentUser.isTelegram) {
    return response.r400(res, config.message.accountNotLinkTelegram)
  }

  if (!code) {
    return response.r400(res, config.message.invalidOTPCode)
  }

  const verify = await TelegramModule.verifyOTP(currentUserId, code)
  if (verify) {
    return response.r400(res, verify)
  }


  if (targetUserId.toString() === currentUserId.toString()) {
    return response.r400(res, config.message.sendGem.cannotSendToYourSelf)
  }

  // if (!isAgency && !isTargetAgency) {
  //   return response.r400(res, config.message.sendGem.cannotSendToUser)
  // }

  // if (isAgency && isTargetAgency) {
  //   return response.r400(res, config.message.sendGem.cannotSendToAgency)
  // }

  if (!isTargetAgency) {
    if (agency == 'level1') {
      return response.r400(res, config.message.sendGem.cannotSendToAgency)
    }
  } else if (targetAgency == 'level1' && agency == 'level1') {
    return response.r400(res, config.message.sendGem.cannotSendToAgency)
  }


  if (isBannedSell) {
    return response.r400(res, config.message.sendGem.cannotSellGem)
  }

  if (isBannedBuy) {
    return response.r400(res, config.message.sendGem.cannotBuyGem(targetUser.username))
  }

  if (gem <= 0) {
    return response.r400(res, config.message.sendGem.invalidGem)
  }


  // Check verification code
  // const isVerificationCodeValid = await VerificationCodeModel.checkCodeValid(currentUser.phone, code)
  // if (!isVerificationCodeValid) {
  //   return response.r400(res, message.invalidOTPCode)
  // }
  let sendFee = 0
  if (gem >= 20000) {
    const { thresholdToChargeFee = 0, feePercent = 0 } = cfg.agency
    console.log(feePercent)
    if (!isAdmin && gem >= thresholdToChargeFee) {
      sendFee = Math.round(gem * feePercent)
    }
  }
  if (isAgency || isAdmin) {
    sendFee = 0
  } else if (isNormalUser && currentUser.gem < gem) {
    return response.r400(res, config.message.sendGem.notEnoughGem)
  }

  let exchangeRate = 0
  if (isAgency) {
    exchangeRate = currentUser.info.exchangeRateSell
  }

  const gemFee = gem + sendFee

  const lostGem = gemFee * -1

  // If agency -> check gem
  const notEnoughGem = await UserModel.checkEnoughGem(currentUser, gemFee)

  if (!isAdmin && !notEnoughGem) {
    return response.r400(res, config.message.sendGem.notEnoughGem)
  }
  // if (isAgency) {
  //   lostGem = gem * -1
  // } else if (isNormalUser) {
  //   lostGem = Math.round(gem * -1 - sendFee)
  // }

  const { error } = await GemHistoryModel.newDoc({
    from: currentUserId,
    to: targetUserId,
    gem,
    note,
    fee: sendFee,
    exchangeRate,
    fromCurrentGem: currentUser.gem + lostGem,
    toCurrentGem: targetUser.gem + gem,
    createdAt: new Date(),
    isFromUserAgency: isAgency,
    isToUserAgency: isTargetAgency,
  })
  console.log(error)
  if (error) {
    return response.r400(res, getError(error))
  }

  await UserModel.updateDocById(currentUserId, { $inc: { gem: lostGem } })

  // Update target gem
  await UserModel.updateDocById(targetUserId, { $inc: { gem } })
  let vpoint = 0
  if ((!isAgency && isTargetAgency) || (isAgency && !isTargetAgency)) {
    if (gem > 75000) {
      vpoint = parseInt(gem / 75000)
      if (isAgency) { await UserModel.updateDocById(currentUserId, { $inc: { vpoint } }) }

      if (isTargetAgency) { await UserModel.updateDocById(targetUserId, { $inc: { vpoint } }) }
    }
  }


  const messageToReceiver = `Người chơi ${currentUser.username} chuyển đến ${currencyFormate(gem)}, số dư ${currencyFormate((targetUser.gem + gem))}. Thời gian ${new Date()}ND: ${note}`
  const messageToSender = `Chuyển cho ${targetUser.username} số tiền ${currencyFormate(gem)}, số dư ${currencyFormate((currentUser.gem + lostGem))}, Phí: ${currencyFormate(sendFeesudo)}. Thời gian ${new Date()}ND: ${note}`

  console.log(messageToReceiver);
  console.log(messageToSender)

  TelegramModule.sendMessage(currentUser, messageToSender)
  TelegramModule.sendMessage(targetUser, messageToReceiver)

  return response.r200(res, {
    userGem: currentUser.gem + lostGem,
    vpoint: currentUser.vpoint + vpoint,
  })
}

const currencyFormate = (gem) => {
  return currencyFormater.format(gem, {
    symbol: '',
    thousand: '.',
    precision: 1,
    format: '%v %s', // %s is the symbol and %v is the value
  })
}

export default {
  sendGem,
}

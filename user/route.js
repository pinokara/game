/**
 * User routes
 * prefix: /users
 */

import express from 'express'
import middleware from '../system/middleware'
import { preQuery } from '../../utils'
import UserCtrl from './controller'

const router = express.Router()

/**
 * @apiDefine Header
 * @apiHeader {string} Authorization Access token
 */

/**
 * @api {post} /users/:userId/send-gem Send gem
 * @apiUse Header
 * @apiGroup User
 * @apiName Send gem
 *
 * @apiParam {number} gem Larger than 0
 * @apiParam {string} note
 * @apiParam {string} code
 */
router.post('/:userId/send-gem', middleware.requiresLogin, UserCtrl.sendGem)

// Middleware
router.param('userId', preQuery.user)

export default router

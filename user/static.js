import lodash from 'lodash'
import config from '../../configs'
import { UserModel, ConfigModel } from '../model'
import { to, helper, format, validation } from '../../utils'
import dbQuery from './query'

const newDoc = async (user) => {
  const doc = new UserModel(user)
  const result = await doc.save()
  return result
}

const findOneByCondition = async (condition) => {
  const query = dbQuery.findByCondition(condition)
  console.log(query)
  const { data } = await to(UserModel.findOne(query))
  return data
}

/**
 * Get user online info
 *
 * @param {Object} data user data
 */
const onlineInfo = async (data) => {
  if (!data) return {}

  const obj = lodash.pick(data, [
    '_id', 'username', 'online', 'isBot', 'verified','accessToken','isBot'
  ])
  if (data.role === 'admin') {
    obj.role = 1
  } else if (data.agency && data.agency !== '') {
    obj.role = 2
  } else {
    obj.role = 3
  }
  return obj
}


/**
 * Get user short info
 *
 * @param {Object} data user data
 */
const shortInfo = async (data) => {
  if (!data) return {}

  return lodash.pick(data, [
    '_id', 'username', 'gem', 'agency', 'role', 'verified', 'socket_id','accessToken', 'isTelegram','telegram_id','isBot'
  ])
}

/**
 * Get user detail info
 *
 * @param {Object} data user data
 */
const detailInfo = async (data) => {
  if (!data) return {}

  return lodash.pick(data, [
    '_id', 'username', 'gem', 'lastActivatedAt', 'remoteIP', 'verified', 'phone', 'wheelSpinTimes', 'isTelegram', 'vpoint','telegram_id','isBot'
  ])
}

/**
 * Get user info by id
 *
 * @param {ObjectId} _id
 */
const getById = async (_id) => {
  if (!_id) return {}

  const query = dbQuery.findByCondition({ _id })
  const { error, data } = await to(UserModel.findOne(query).lean())
  if (error) return {}

  const obj = await shortInfo(data)
  return obj
}

/**
 * Get user detail info by id
 *
 * @param {ObjectId} _id
 */
const detailById = async (_id) => {
  if (!_id) return {}

  const query = dbQuery.findByCondition({ _id })
  const { error, data } = await to(UserModel.findOne(query).lean())
  if (error) return {}

  const obj = await detailInfo(data)
  return obj
}

/**
 * Get full data by id
 *
 * @param {ObjectId} _id
 */
const fullById = async (_id) => {
  if (!_id) return {}

  const query = dbQuery.findByCondition({ _id })
  const { data } = await to(UserModel.findOne(query).lean())
  return data
}

// /**
//  * Check user current gem reach minimum gem or not
//  *
//  * @param {Object} user
//  * @param {Number} minGem mimimun gem
//  */
// const checkEnoughGem = (user, minGem) => {
//   return user && user.gem >= minGem
// }

/**
 * Check user current gem reach minimum gem or not
 *
 * @param {Object} user
 * @param {Number} minGem mimimun gem
 */
const checkEnoughGem = async (user, minGem) => {
  const { data } = await to(UserModel.findOne({ _id: user._id, gem: { $gte: minGem } }).lean())
  // console.log("CHECK ENOUGH GEM: "+ user._id +" gem "+ minGem+ " result "+ JSON.stringify(data, null, 2))
  return data != null
}

/**
 * Check user is banned from game or not
 *
 * @param {Object} user
 * @param {String} gameType
 */
const checkBannedGame = (user, gameType) => {
  return user && user.bannedGames && user.bannedGames.includes(gameType)
}

/**
 * Update by id
 *
 * @param {ObjectId}  _id   user id
 * @param {Object}    body  update data
 */
const updateDocById = async (_id, payload) => {
  const result = await to(UserModel.updateOne({ _id }, payload))
  return result
}

/**
 * Pick random bot for games
 */
const pickRandomBot = async () => {
  const totalBots = await to(UserModel.countDocuments({ isBot: true }))

  if (!totalBots.data) {
    return null
  }

  // Pick a random number
  const rand = helper.randomIntegerInRange(0, parseInt(totalBots.data, 10) - 1)
  const { data } = await to(UserModel.findOne({ isBot: true }).skip(rand).lean())
  return data
}

/**
 * Pick random bots for games
 */
const pickRandomBots = async (ignoreIds = [], limit = 10) => {
  const totalBots = await to(UserModel.countDocuments({ isBot: true }))

  if (!totalBots) {
    return null
  }

  // Pick a random number
  const rand = helper.randomIntegerInRange(0, parseInt(totalBots, 10) - 1)
  const { data } = await to(UserModel.find({ isBot: true, _id: { $nin: ignoreIds } }).skip(rand).limit(limit).lean())
  return data
}

/**
 * Update user game stata
 *
 * @param {Object} history game history data
 */
const updateGameStats = async (history) => {
  const { user, gameType, isWin, winGem, betValue } = history

  const payload = {
    $inc: {
      [`gameStats.${gameType}.totalGame`]: 1,
      [`gameStats.${gameType}.totalBetValue`]: betValue,
      [`gameStats.${gameType}.totaWin`]: isWin ? 1 : 0,
      [`gameStats.${gameType}.winGem`]: winGem,
    },
  }
  await UserModel.updateDocById(user, payload)
}

/**
 * Get game ranking by game type
 *
 * @param {String} gameType
 */
const getRankingByGameType = async (gameType) => {
  const { data } = await to(UserModel.find({
    [`gameStats.${gameType}`]: { $exists: true },
  }).sort({
    [`gameStats.${gameType}.winGem`]: -1,
  }).limit(config.limit.l20).lean())

  if (!data || !data.length) return []

  const result = await Promise.all(data.map(async (item) => {
    const obj = await shortInfo(item)
    obj.stats = item.gameStats[gameType]
    return obj
  }))
  return result
}

/**
 * Get list agencies
 */
const getListAgencies = async () => {
  const { data } = await to(UserModel.find({ agency: 'level1', 'info.displayOnApp': true }).lean())
  const result = data.map((item) => {
    const obj = {
      ...lodash.pick(item, ['_id', 'username', 'nickname', 'agency','accessToken']),
      ...item.info,
    }
    return obj
  })
  return result
}

/**
 * Check phone existed
 *
 * @param {String} phone
 */
const checkPhoneExisted = async (phone) => {
  phone = format.phone(phone)
  const { data } = await to(UserModel.countDocuments({ phone, verified: true }))
  return data > 0
}

/**
 * Check username existed
 *
 * @param {String} username
 */
const checkUsernameExisted = async (username = '') => {
  if (username.length < 6) {
    return true
  }

  // if (username.length == 6) {
  //   const { data } = await to(UserModel.countDocuments({ username }))
  //   return data > 0
  // } else {
  //   // const { data } = await to(UserModel.countDocuments({ username: format.searchString(username.substring(0, 4)) }))
  //   let subName = username.substring(0, 6);
  //   //{username:{$regex : ".*doantra.*"}
  //   //const { data } = await to(UserModel.countDocuments({ username }))
  //   const { data } = await to(UserModel.countDocuments({ username: { $regex: subName + ".*" } }))
  //   return data > 0
  // }
  const { data } = await to(UserModel.countDocuments({ username }))
  return data > 0
}

/**
 * Set bot online
 */
const setBotOnline = async () => {
  // Reset all bots first
  await UserModel.updateMany({ isBot: true }, { $set: { online: false } })

  const cfgDoc = await ConfigModel.getConfig()
  const { botOnline = 0 } = cfgDoc
  if (botOnline === 0) return

  const countBot = await to(UserModel.countDocuments({ isBot: true }))
  const rest = countBot.data - botOnline
  // If rest < 0, update all bots to online
  if (rest < 0) {
    await UserModel.updateMany({}, { $set: { online: true } })
    return
  }

  // Random from 0 to rest
  const rand = helper.randomIntegerInRange(0, rest)
  const bots = await to(UserModel.find({ isBot: true }).skip(rand).limit(botOnline))
  for (const bot of bots.data) {
    /* eslint no-await-in-loop:0 */
    await bot.update({ online: true })
  }
  return
}

/**
 * Get brief info by user id
 *
 * @param {String} userId user id
 */
const getBriefInfoById = async (userId) => {
  if (!validation.isObjectId(userId)) {
    return null
  }
  const user = await findOneByCondition({ _id: userId })
  if (!user) {
    return null
  }
  const result = await briefInfo(user)
  return result
}

/**
 * Brief info for user
 *
 * @param {Object} user
 */
const briefInfo = async (user) => {
  return lodash.pick(user, [
    '_id', 'username', 'nickname', 'role', 'createdAt', 'gem', 'isBot', 'setAt', 'agency', 'info', 'verified', 'phone', 'isTelegram', 'vpoint','accessToken'
  ])
}

export default {
  newDoc,
  findOneByCondition,
  onlineInfo,
  getById,
  detailById,
  fullById,
  checkEnoughGem,
  checkBannedGame,
  updateDocById,
  pickRandomBot,
  pickRandomBots,
  updateGameStats,
  getRankingByGameType,
  getListAgencies,
  checkPhoneExisted,
  checkUsernameExisted,
  setBotOnline,
  getBriefInfoById,
}

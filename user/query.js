import { ObjectId } from '../../utils/mongoose'
import { validation, format } from '../../utils'

const findByCondition = ({ _id, username, phone, verified, telegram_id, online, remoteIP }) => {
  const condition = {}
  if (validation.isObjectId(_id)) {
    condition._id = new ObjectId(_id)
  }

  if (username) {
    condition.username = username
  }

  if (phone) {
    condition.phone = format.phone(phone)
  }

  if (validation.isBoolean(verified)) {
    condition.verified = verified
  }

  if (telegram_id) {
    condition.telegram_id = telegram_id
  }

  if (online) {
    condition.online = online
  }

  if (remoteIP) {
    condition.remoteIP = remoteIP
  }

  return condition
}

export default {
  findByCondition,
}

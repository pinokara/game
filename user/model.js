import crypto from 'crypto'
import uniqueValidator from 'mongoose-unique-validator'
import { Schema, mongoose } from '../../utils/mongoose'
import configs from '../../configs'
import statics from './static'
import { format } from '../../utils';
import { UserModel } from '../model';

const { message } = configs

const schema = new Schema({
    username: {
        type: String,
        unique: true,
        required: [true, message.usernameRequired],
    },
    isTelegram: {
        type: Boolean,
        default: false,
    },
    nickname: String,
    telegram_id: String,
    phone: {
        type: String,
        unique: true,
    },
    hashedPassword: String,
    facebookId: String,
    salt: String,
    gem: {
        type: Number,
        // default: process.env.NODE_ENV === 'production' ? 0 : 0,
        default: 0,
    },
    socket_id: {
        type: String,
        default: ''
    },
    vpoint: {
        type: Number,
        default: 0,
    },
    wheelSpinTimes: {
        type: Number,
        default: 0,
    },
    verified: {
        type: Boolean,
        default: false,
    },
    isBlock: {
        type: Boolean,
        default: false,
    },
    role: {
        type: String,
        default: 'user',
    },
    lastActivatedAt: {
        type: Date,
        default: Date.now,
    },
    remoteIP: {
        type: String,
        default: '',
    },
    isBot: {
        type: Boolean,
        default: false,
    },
    info: {},
    agency: String,
    gameStats: {
        bigSmall: {
            totalGame: {
                type: Number,
                default: 0
            },
            totalBetValue: {
                type: Number,
                default: 0
            },
            totaWin: {
                type: Number,
                default: 0
            },
            winGem: {
                type: Number,
                default: 0
            },
        },
        slotLienQuan: {
            totalGame: {
                type: Number,
                default: 0
            },
            totalBetValue: {
                type: Number,
                default: 0
            },
            totaWin: {
                type: Number,
                default: 0
            },
            winGem: {
                type: Number,
                default: 0
            },
        },
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    bannedGames: [String],
    bannedTransferGem: [String],
    accessToken: String,
    online: {
        type: Boolean,
        default: true,
    },
}, {
    versionKey: false,
})

schema.index({ isBot: 1 }).index({ gameStats: 1 }).index({ agency: 1 })

/**
 * Virtual
 */
schema.virtual('password').set(function (password) {
    this._password = password
    this.salt = this.makeSalt()
    this.hashedPassword = this.hashPassword(password)
}).get(function () {
    return this._password
})

/**
 * Methods
 */
schema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} plainText
     */
    authenticate(plainText) {
        return this.hashPassword(plainText) === this.get('hashedPassword')
    },

    /**
     * Make salt
     *
     * @return {String}
     */
    makeSalt() {
        return crypto.randomBytes(16).toString('base64')
    },

    /**
     * Hash password
     *
     * @param {String} password
     */
    hashPassword(password) {
        if (!password || !this.get('salt')) return ''
        const salt = Buffer.from(this.get('salt'), 'base64')
        return crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha1').toString('base64')
    },
    updateTelegram(currentUserId, telegram_id) {
        if (telegram_id) {
            this.updateDocById(currentUserId, {
                telegram_id,
                isTelegram: true
            })
        }
    },
}

/**
 * Static functions
 *
 */
schema.statics = statics

/**
 * Presave hook
 */
schema.pre('save', function (next) {
    // XSS name
    this.name = format.xssString(this.name)

    this.username = this.username.toLowerCase()

    next()
})


schema.statics.upsertFbUser = function (accessToken, refreshToken, profile, cb) {
    var that = this;
    return this.findOne({
        'facebookId': profile.id
    }, function (err, user) {
        // no user was found, lets create a new one
        if (!user) {
            var newUser = new that({
                username: profile.id,
                facebookId: profile.id,
                nickname: profile.displayName
            });

            newUser.save(function (error, savedUser) {
                if (error) {
                    console.log(error);
                }
                return cb(error, savedUser);
            });
        } else {
            return cb(err, user);
        }
    });
};

schema.index({ username: 1 }).index({ isBot: 1 })

schema.plugin(uniqueValidator, { message: 'Tên đăng nhập hoặc SDT đã được đăng ký!' })

export default mongoose.model('User', schema)

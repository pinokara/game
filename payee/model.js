import {Schema, mongoose} from '../../utils/mongoose'
import statics from './static'
import {format, constant} from '../../utils'

const {REQUEST, PROCESSING, FAILED, SUCCESS} = constant

const schema = new Schema({
    merchantNo: {
        type: String
    },
    orderNo: {
        type: String
    },
    userNo: Number,
    userId: {
        type: String,
    },
    userName: {
        type: String,
    },
    channelNo: {
        type: Number
    },
    amount: {
        type: Number
    },
    discount: {
        type: Number
    },
    payeeName: {
        type: String
    },
    bankName: {
        type: String
    },
    extra: String,
    datetime: String,
    time: Number,
    status: {
        type: String,
        enum: [REQUEST, PROCESSING, FAILED, SUCCESS],
        default: REQUEST
    },
    createDate: {
        type: Date,
    },
    updateDate: {
        type: Date,
        default: Date.now
    }
})

/**
 * Static functions
 *
 */
schema.statics = statics

export default mongoose.model('payee', schema)

import crypto from 'crypto'

const NOTIFYURL = process.env.THEPAY_NOTIFYUR_PAYEE
const MERCHANT_NO = process.env.THEPAY_MERCHANT_NO
const KEY = process.env.THEPAY_KEY
const encryptData = (data) => {
  // crypto.createHash('2')
  var sign = 'amount=' + parseInt(data.amount) + '&datetime=' + data.datetime + '&discount=' + parseInt(data.discount) + '&extra=' + data.extra + '&merchantNo=' + MERCHANT_NO + '&notifyUrl=' + NOTIFYURL + '&orderNo=' + data._id + '&time=' + data.time + '&userNo=' + data.userNo + KEY
  console.log('encrypt data', data, sign)
  var parseHash = crypto.createHash('sha256').update(sign, 'utf8').digest('hex');
  console.log("SHA256", parseHash)
  return crypto.createHash('md5').update(parseHash, 'utf8').digest('hex').toLocaleUpperCase();
}

module.exports = {
  encryptData
}

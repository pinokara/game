import {response, getError} from '../../utils'
import {PayeeModel, PayoutModel, UserModel} from '../../packages/model'
import utils from './utils'
import lodash from 'lodash'
import moment from 'moment'
import requestHTTP from 'request'
import config from '../../configs'
import {format, constant, helper} from '../../utils';
import Pusher from 'pusher'

const {REQUEST, PROCESSING, FAILED, SUCCESS} = constant
const MERCHANT_NO = process.env.THEPAY_MERCHANT_NO
const NOTIFYURL = process.env.THEPAY_NOTIFYUR_PAYEE
const APPSECRET = process.env.THEPAY_APPSECRET
const APIURL = process.env.THEPAY_APIURL
const CONTENT_TYPE = 'application/json';
const ACCEPT = 'application/json';
const moneyArr = [100000, 200000, 500000, 1000000];

const pusher = new Pusher({
    appId: process.env.PUSHER_APP_ID,
    key: process.env.PUSHER_APP_KEY,
    secret: process.env.PUSHER_APP_SECRET,
    cluster: process.env.PUSHER_APP_CLUSTER,
});

const createOrder = async (req, res) => {
    let user = req.user
    let data = req.body
    let userNo = Math.floor(Date.now() / 1000)
    let amount = parseInt(data.amount);
    if (!moneyArr.includes(amount) && amount < 1000000) {
        return response.r400(res, 'Số tiền đầu vào không hợp lệ')
    }
    let thePay = {
        merchantNo: MERCHANT_NO,
        userNo: userNo,
        userName: user.username,
        userId: user._id,
        channelNo: 4,
        amount: amount,
        discount: 0,
        payeeName: data.fullName,
        bankName: data.bankName,
        extra: data.extra,
        datetime: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
        time: Math.floor(Date.now() / 1000),
    }
    const result = await PayeeModel.newDoc(thePay)
    let sign = utils.encryptData(result)
    console.log('Parsse sign:', sign);
    result.sign = sign
    await PayeeModel.updateDocById(result._id, result);

    result.orderNo = result._id.toString()
    result.notifyUrl = NOTIFYURL
    result.appSecret = APPSECRET

    let requestData = lodash.pick(result, ['merchantNo', 'orderNo', 'userNo', 'channelNo', 'amount', 'discount', 'payeeName', 'bankName', 'extra', 'datetime', 'notifyUrl', 'time', 'appSecret', 'sign'])
    console.log('Request Data', requestData)
    let url = APIURL + '/order/create'
    requestHTTP.post(url, {
        form: requestData,
        headers: {
            content_type: CONTENT_TYPE,
            accept: ACCEPT
        }
    }, async function (err, httpRes, body) {

        if (err) {
            console.log(err)
            return response.r400(res, config.message.serverError)
        }
        if (body) {
            let result = JSON.parse(body)
            if (result.code === 0) {
                var orderNo = result.orderNo
                var checkPay = await PayeeModel.fullById(orderNo)
                if (checkPay && checkPay.status === REQUEST) {
                    let resData = {
                        code: result.code,
                        text: result.text,
                        payable: result.payable,
                        targetUrl: result.targetUrl,
                        qrcode: result.qrcode
                    }
                    return response.r200(res, resData)
                } else {
                    return response.r400(res, 'Dữ liệu không tồn tại')
                }
            } else {
                return response.r400(res, result.text)
            }
        }
    })
}

const notifyApi = async (req, res) => {
    let body = req.body
    // console.log("notifyApi", body);
    let result = JSON.parse(JSON.stringify(body))
    console.log(result);
    if (result.status === 'PAID') {
        var checkPay = await PayeeModel.fullById(result.orderNo)
        // console.log('CheckPay', checkPay)
        // console.log('check status ', checkPay.status)
        // console.log('CheckPay Stringfy ', JSON.stringify(checkPay.status))
        var status = JSON.stringify(checkPay.status)
        var check = status !== FAILED || status !== SUCCESS

        console.log('check status checkPay.status !== FAILED || checkPay.status !== SUCCESS', check)
        if (check) {
            var payee = await PayeeModel.updateDocById(result.orderNo, {
                status: SUCCESS,
                tradeNo: result.tradeNo
            })
            let amountPayTmp = parseInt(checkPay.amount)
            let amountPay = amountPayTmp * 2;
            // let mountBounus = 0
            //
            // let amountPay = 0;
            // switch (amountPayTmp) {
            //     case 100000:
            //         amountPay = 110000
            //         mountBounus = 140000
            //         break;
            //     case 200000:
            //         amountPay = 230000
            //         mountBounus = 270000
            //         break;
            //     case 500000:
            //         amountPay = 580000
            //         mountBounus = 670000
            //         break;
            //     case 1000000:
            //         amountPay = 1175000
            //         mountBounus = 1325000
            //         break;
            //     default:
            //         amountPay = amountPay + amountPay * 0.176;
            //         mountBounus = (amountPay * 1.5) - (amountPay * 0.176)
            // }
            console.log('update payee', payee)
            console.log('Amount', amountPay)
            await UserModel.updateDocById(checkPay.userId, {$inc: {gem: amountPay}})
            var user = await UserModel.getById(checkPay.userId)
            console.log('User:', user)
            console.log('money-event')
            pusher.trigger('money-event', 'money-event-' + user._id, {money: user.gem});
            // Send giftcode
            // console.log('Send giftcode 1,5', mountBounus)
            // helper.sendBonusGiftCode(user._id, mountBounus)
        }
    } else {
        // return response.r400(res, 'Có lỗi trong quá trình thực hiện')
        return res.send('error')
    }
    return res.send('success')
}

export default {
    createOrder,
    notifyApi
}

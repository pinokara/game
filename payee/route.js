import express from 'express'
import ThePayCtrl from './controller'
import middleware from "../system/middleware";

const router = express.Router()
//middleware.requiresLogin,
router.post("/create", middleware.requiresLogin, ThePayCtrl.createOrder)
router.post("/notify", ThePayCtrl.notifyApi)

export default router

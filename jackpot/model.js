import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  gameType: {
    type: String,
    default: '',
  },
  value: {
    type: Number,
  },
  minimumValue: {
    type: Number,
  },
  percentWinnerTake: {
    type: Number,
  },
  percentBookieTake: {
    type: Number,
  },
  thresholdToWin: {
    type: Number,
    default: 10000,
  },
}, {
  collection: 'jackpots',
  versionKey: false,
})

schema.statics = statics

export default mongoose.model('Jackpot', schema)

import { to } from '../../utils'
import { JackpotModel, BetValueModel } from '../model'

/**
 * Get game jackpots and bets data
 *
 * @param {String} gameType
 */
const getGameJackpotData = async (gameType) => {
  let betValues = []

  const { data } = await to(JackpotModel.find({ gameType }).lean())
  if (!data || !data.length) return { jackpotValues: [], betValues }

  // Find bet values for each
  const result = await Promise.all(data.map(async (item) => {
    const bets = await to(BetValueModel.find({ jackpot: item._id }).lean())
    item.betValues = bets.data || []
    betValues = betValues.concat(item.betValues)
    return item
  }))

  return {
    jackpotValues: result,
    betValues,
  }
}

export default {
  getGameJackpotData,
}

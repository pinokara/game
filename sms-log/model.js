import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  phone: String,
  content: String,
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

schema.index({ phone: 1 })

schema.statics = statics

export default mongoose.model('SMSLog', schema)

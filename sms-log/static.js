import { to } from '../../utils'
import { SMSLogModel } from '../model'

/**
 * New doc
 *
 */
const newDoc = async (payload) => {
  const doc = new SMSLogModel(payload)
  const result = await to(doc.save())
  return result
}

export default {
  newDoc,
}

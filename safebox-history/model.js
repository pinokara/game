import { Schema, mongoose } from '../../utils/mongoose'

const schema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
  },
  gem: {
    type: Number,
    // default: process.env.NODE_ENV === 'production' ? 0 : 0,
    default: 0,
  },
  updateDate: {
    type: Date,
    default: Date.now,
  },
  action: {
    type: String,
    enum: ['WITHDRAW', 'DEPOSIT'],
  },

})

export default mongoose.model('safebox-history', schema)

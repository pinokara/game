/**
 * Me routes
 * prefix: /me
 */

import express from 'express'
import middleware from '../system/middleware'
import SafeBoxCtl from './controller'

const router = express.Router()

/**
 * @apiDefine Header
 * @apiHeader {string} Authorization Access token
 */

/**
 * @api {post} /withdraw withdraw gem from safebox
 * @apiUse Header
 * @apiGroup withdraw gem from safebox
 * @apiName withdraw gem from safebox
 */
router.post('/withdraw', middleware.requiresLogin, SafeBoxCtl.withdraw)

/**
 * @api {post} /deposit deposit gem into safebox
 * @apiUse Header
 * @apiGroup deposit gem into safebox
 * @apiName deposit gem into safebox
 */
router.post('/deposit', middleware.requiresLogin, SafeBoxCtl.deposit)

/**
 * @api {get} /getSafeBox get gem in safebox
 * @apiUse Header
 * @apiGroup get gem into safebox
 * @apiName get gem into safebox
 */
router.get('/getSafeBox', middleware.requiresLogin, SafeBoxCtl.getSafeGem)

export default router

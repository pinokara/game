import { ObjectId } from '../../utils/mongoose'
import { validation, format } from '../../utils'

const findByCondition = ({ _id, username, phone, verified, user }) => {
  const condition = {}
  if (validation.isObjectId(_id)) {
    condition._id = new ObjectId(_id)
  }

  if (username) {
    condition.username = username
  }

  if(user){
    condition.user = new ObjectId(user)
  }

  if (phone) {
    condition.phone = format.phone(phone)
  }

  if (validation.isBoolean(verified)) {
    condition.verified = verified
  }

  return condition
}

export default {
  findByCondition,
}

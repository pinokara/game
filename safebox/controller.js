import config from '../../configs'
import { response, getError, helper } from '../../utils'
import { UserModel, SafeBox, SafeBoxHistory } from '../../packages/model'
import { TelegramModule } from '../../modules'

const _ = require('lodash');

const withdraw = async (req, res) => {
  const currentUserId = req.user._id
  const { gem, code } = _.pick(req.body,
    ['gem', 'code']);
  if (gem <= 0) {
    console.log(config.message.sendGem.invalidGem)
    return response.r400(res, config.message.sendGem.invalidGem)
  }
  const verifyOTP = await TelegramModule.verifyOTP(currentUserId, code);
  if (verifyOTP) {
    return response.r400(res, verifyOTP)
  }

  const currentUser = await UserModel.findOneByCondition({ _id: currentUserId })
  if (!currentUser.isTelegram) {
    console.log(config.message.noPermission)
    return response.r400(res, config.message.noPermission)
  }

  let safeBox = await SafeBox.findOneByCondition({ user: currentUserId });
  if (!safeBox) {
    safeBox = new SafeBox({
      user: currentUser,
      gem,
    })
    await safeBox.save();
  }
  if (safeBox.gem < gem) {
    return response.r400(res, config.message.sendGem.invalidGem)
  }
  const lostGem = Math.round(gem * -1)
  const totalGem = safeBox.gem + lostGem;
  await SafeBox.updateDocById(safeBox._id, { $inc: { gem: lostGem } })
  await UserModel.updateDocById(currentUserId, { $inc: { gem } })

  const safeBoxHistory = new SafeBoxHistory({
    user: currentUser,
    gem,
    action: 'WITHDRAW',
  });
  await safeBoxHistory.save();

  return response.r200(res, { updateGeme: lostGem, totalGem })
}
const deposit = async (req, res) => {
  const currentUserId = req.user._id
  const { gem } = _.pick(req.body,
    ['gem']);
  if (gem <= 0) {
    return response.r400(res, config.message.sendGem.invalidGem)
  }
  const currentUser = await UserModel.findOneByCondition({ _id: currentUserId })
  let safeBox = await SafeBox.findOneByCondition({ user: currentUserId });
  if (!safeBox) {
    safeBox = new SafeBox({
      user: currentUser,
      gem: 0,
    })
    await safeBox.save()
  }
  if (currentUser.gem < gem) {
    return response.r400(res, config.message.sendGem.invalidGem)
  }
  const totalGem = parseInt(safeBox.gem) + parseInt(gem);
  console.log(totalGem);
  await SafeBox.updateDocById(safeBox._id, { $inc: { gem } })
  const lostGem = Math.round(gem * -1)
  await UserModel.updateDocById(currentUserId, { $inc: { gem: lostGem } })

  const safeBoxHistory = new SafeBoxHistory({
    user: currentUser,
    gem,
    action: 'DEPOSIT',
  });
  await safeBoxHistory.save();

  return response.r200(res, { updateGeme: gem, totalGem })
}

const getSafeGem = async (req, res) => {
  const currentUserId = req.user._id
  console.log("getSafeGem", currentUserId)
  const safeBox = SafeBox.findOneByCondition({ user: currentUserId });
  safeBox.then((box) => {
    return response.r200(res, box)
  }).catch((err) => {
    return response.r400(res, getError.message(err))
  })
}

export default {
  withdraw,
  deposit,
  getSafeGem,
}

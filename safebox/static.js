import lodash from 'lodash'
import config from '../../configs'
import { SafeBox, ConfigModel } from '../model'
import { to, helper, format, validation } from '../../utils'
import dbQuery from './query'


const findOneByCondition = async (condition) => {
  const query = dbQuery.findByCondition(condition)
  console.log("SafeBox query", query)
  const { data } = await to(SafeBox.findOne(query))
  return data
}

/**
 * safebox by id
 *
 * @param {ObjectId}  _id   safebox id
 * @param {Object}    body  update data
 */
const updateDocById = async (_id, payload) => {
  const result = await to(SafeBox.updateOne({ _id }, payload))
  return result
}

export default {
  findOneByCondition,
  updateDocById,

}

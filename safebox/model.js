import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'user',
  },
  gem: {
    type: Number,
    // default: process.env.NODE_ENV === 'production' ? 0 : 0,
    default: 0,
  },
})
schema.statics = statics


export default mongoose.model('safebox', schema)

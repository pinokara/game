import lodash from 'lodash'
import { to } from '../../utils'
import { InboxMessageModel, InboxStatusModel, InboxModel } from '../model'

/**
 * Get inboxes of user
 *
 */
const getByUserId = async (userId, { page, limit }) => {
  const { data } = await to(InboxStatusModel.find({ user: userId }).sort('-updatedAt').skip(page * limit).limit(limit).lean())

  const result = await Promise.all(data.map(async (item) => {
    const obj = await detailInfo(item)
    return obj
  }))
  return result
}

/**
 * Count total inboxes of user
 *
 */
const countByUserId = async (userId) => {
  const { data } = await to(InboxStatusModel.countDocuments({ user: userId }))
  return data
}

/**
 * Detail info
 *
 */
const detailInfo = async (data) => {
  const result = await Promise.all([{
    users: await InboxModel.getUsersInInbox(data.inbox, data.user),
    lastMessage: await InboxMessageModel.getLastMessage(data.inbox),
  }])

  return Object.assign(
    lodash.pick(data, ['_id', 'updatedAt', 'unread', 'inbox']),
    result[0],
  )
}

/**
 * Update status of sender
 */
const updateStatusOfSender = async (inboxId, userId) => {
  await InboxStatusModel.findOneAndUpdate({ inbox: inboxId, user: userId }, {
    $set: {
      updatedAt: Date.now(),
      unread: 0,
    },
  })
}

/**
 * Update status of receiver
 */
const updateStatusOfReceiver = async (inboxId, senderId) => {
  await InboxStatusModel.findOneAndUpdate({ inbox: inboxId, user: { $ne: senderId } }, {
    $set: {
      updatedAt: Date.now(),
    },
    $inc: {
      unread: 1,
    },
  })
}

/**
 * Seen inbox
 */
const seen = async (inboxId, userId) => {
  await InboxStatusModel.findOneAndUpdate({ inbox: inboxId, user: userId }, {
    $set: {
      unread: 0,
      lastSeenAt: Date.now(),
    },
  })
}

export default {
  getByUserId,
  countByUserId,
  detailInfo,
  updateStatusOfSender,
  updateStatusOfReceiver,
  seen,
}

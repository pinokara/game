import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  inbox: {
    type: Schema.Types.ObjectId,
    ref: 'Inbox',
  },
  lastSeenAt: Date,
  updatedAt: Date,
  unread: {
    type: Number,
    default: 0,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

schema.index({ user: 1 }).index({ inbox: 1 })

schema.statics = statics

export default mongoose.model('InboxStatus', schema)

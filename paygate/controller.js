import { response } from '../../utils'
import PayGateModel from './model';


const lists = async (req, res) => {
  const data = [{
    banks: await PayGateModel.find({ type: 1 }),
    wallets: await PayGateModel.find({ type: 2 }),
    cards: await PayGateModel.find({ type: 3 }),
    th: await PayGateModel.find({ type: 4 })
  }]
  return response.r200(res, data)
}


export default {
  lists
}

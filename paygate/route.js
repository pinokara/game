/**
 * Game routes
 * prefix: /games
 */

import express from 'express'
import middleware from '../system/middleware'
import PayGateCtl from './controller'

const router = express.Router()


router.get('/lists', middleware.requiresLogin, PayGateCtl.lists)

export default router

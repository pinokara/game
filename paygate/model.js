import {Schema, mongoose} from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
    name: String,
    fullname: String,
    logo: String,
    type: Number,
    accounts: Schema.Types.Mixed,
    status: {
        type: Number,
        default: 0,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
}, {
    versionKey: false,
})

schema.index({status: 1})

schema.statics = statics

export default mongoose.model('paygates', schema)

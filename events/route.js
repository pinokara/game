/**
 * Event routes
 * prefix: /events
 */

import express from 'express'
import EventCtrl from './controller'
import middleware from '../system/middleware'

const router = express.Router()


/**
 * @api {get} /event get all event
 * @apiGroup Event
 * @apiName get all
 *
 */
router.get('/', middleware.requiresLogin, EventCtrl.all)

/**
 * @api {get} /event/{eventId} get a event
 * @apiGroup Event
 * @apiName get a event
 *
 */
router.get('/:eventId', middleware.requiresLogin, EventCtrl.show)


export default router

import { response, helper, getError } from '../../utils'
import { EventModel } from '../../packages/model'
import configs from '../../configs'


/**
 * Get all events
 *
 */
const all = async (req, res) => {
  const locale = helper.getLocale(req)
  const { page = 0, sort = '-createAt' } = req.query
  const limit = configs.limit.l10
  // Find
  const data = await Promise.all([{
    event: await EventModel.findByCondition({ ...req.query }, { page, limit }, sort),
    total: await EventModel.countByCondition({ ...req.query }),
    limitPerPage: limit,
  }])

  const result = data[0]

  // // Process users
  // result.e = await Promise.all(result.agencies.map(async (item) => {
  //   const obj = await UserModel.briefInfo(item)
  //   return obj
  // }))
  return response.r200(res, locale, result)
}


/**
 * Show game config
 */
const show = async (req, res) => {
  const locale = helper.getLocale(req)
  const { eventId } = req.params
  const event = await EventModel.findOneByCondition({ _id: eventId, active: true })
  return response.r200(res, locale, { event })
}

export default {
  all,
  show,
}

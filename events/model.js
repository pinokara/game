import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'
import { SLOT, SLOT_JACKPOT, BIG_SMALL, LUCKY_WHEEL, ALL } from '../../utils/constant'

const schema = new Schema({
  name: {
    type: String,
  },
  description: {
    type: String,
  },
  gameType: {
    type: String,
    enum: [SLOT, BIG_SMALL, LUCKY_WHEEL, ALL, SLOT_JACKPOT],
    default: ALL,
  },
  bonus: {
    type: Number,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
  startAt: {
    type: Date,
    default: Date.now,
  },
  endAt: {
    type: Date,
    default: Date.now,
  },
  active: {
    type: Boolean,
    default: false,
  },
}, {
  collection: 'events',
  versionKey: false,
})


schema.statics = statics

export default mongoose.model('Event', schema)

import { validation } from '../../utils'
import { ObjectId } from '../../utils/mongoose'

/**
 * Find condition from object
 *
 * @param {Object} condition allow _id properties
 */
const findByCondition = (condition) => {
  // console.log(condition)
  // const condition = {}
  // _id
  if (condition._id && validation.isObjectId(condition._id)) {
    condition._id = new ObjectId(condition._id)
  }
  condition.active = true
  const date = new Date();
  // const timeCondition = { $and: [{ startAt: { $lte: date } }, { endAt: { $gte: date } }] }

  condition.startAt = {
    $lte: date
  }
  condition.endAt = {
    $gte: date
  }
  // const query = Object.assign(condition, timeCondition);
  // console.log(JSON.stringify(query));
  return condition
}

export default {
  findByCondition,
}

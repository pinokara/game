import { EventModel } from '../model'
import { to } from '../../utils'
import dbQuery from './query'

/**
 * ****************************************
 * QUERY SECTION
 * ****************************************
 */
/**
 * Find event value by condition
 *
 * @param {Object} condition query condition
 */
const findByCondition = async (condition, { page = 0, limit }, sort = '-createdAt') => {
  const query = dbQuery.findByCondition(condition)
  const { data } = await to(EventModel.find(query).skip(page * limit).limit(limit).sort(sort))
  return data || []
}

/**
 * Find one document by condition
 *
 * @param {Object} condition query condition
 */
const findOneByCondition = async (condition) => {
  const query = dbQuery.findByCondition(condition)
  // console.log("EVENT QUERYS", JSON.stringify(query))
  const betValue = await EventModel.findOne(query)
  return betValue
}

/**
 * Count by condition
 *
 * @param {Object} condition find condition
 */
const countByCondition = async (condition) => {
  const query = dbQuery.findByCondition(condition)

  const { data } = await to(EventModel.countDocuments(query))
  return data
}


export default {
  countByCondition,
  findOneByCondition,
  findByCondition,
}

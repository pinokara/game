/**
 * Inbox routes
 * prefix: /inboxes
 */

import express from 'express'
import middleware from '../system/middleware'
import { preQuery } from '../../utils'
import InboxCtrl from './controller'

const router = express.Router()

/**
 * @apiDefine Header
 * @apiHeader {String} Authorization Access token
 */

/**
 * @api {get} /inboxes My inboxes
 * @apiUse Header
 * @apiGroup Inbox
 * @apiName My inboxes
 *
 * @apiParam {number} page Default 0
 */
router.get('/', middleware.requiresLogin, InboxCtrl.all)

/**
 * @api {get} /inboxes/:inboxId Detail
 * @apiUse Header
 * @apiGroup Inbox
 * @apiName Detail
 *
 */
router.get('/:inboxId', middleware.requiresLogin, InboxCtrl.show)

/**
 * @api {get} /inboxes/by-user-id/:userId Show by user id
 * @apiUse Header
 * @apiGroup Inbox
 * @apiName Show by user id
 *
 */
router.get('/by-user-id/:userId', middleware.requiresLogin, InboxCtrl.showByUserId)

/**
 * @api {get} /inboxes/:inboxId/messages Messages
 * @apiUse Header
 * @apiGroup Inbox
 * @apiName Messages
 *
 */
router.get('/:inboxId/messages', middleware.requiresLogin, InboxCtrl.getMessages)

/**
 * @api {post} /inboxes/:inboxId/messages Send message
 * @apiUse Header
 * @apiGroup Inbox
 * @apiName Send message
 *
 * @apiParam {string} content
 */
router.post('/:inboxId/messages', middleware.requiresLogin, InboxCtrl.sendMessage)

// Middleware
router.param('inboxId', preQuery.inbox)

export default router

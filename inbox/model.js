import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  users: [{
    type: Schema.Types.ObjectId,
    ref: 'User',
  }],
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

schema.index({ users: 1 })

schema.statics = statics

export default mongoose.model('Inbox', schema)

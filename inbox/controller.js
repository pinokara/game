import config from '../../configs'
import { response } from '../../utils'
import { InboxStatusModel, InboxModel, InboxMessageModel } from '../../packages/model'

/**
 * All inboxes
 */
const all = async (req, res) => {
  const userId = req.user._id
  const { page } = req.query
  const limit = config.limit.l20

  const result = await Promise.all([{
    inboxes: await InboxStatusModel.getByUserId(userId, { page, limit }),
    total: await InboxStatusModel.countByUserId(userId),
    limit,
  }])

  // Return
  return response.r200(res, result[0])
}

/**
 * Get inbox info
 *
 */
const show = async (req, res) => {
  const userId = req.user._id
  const inbox = await InboxModel.detailInfo(req.inboxData, userId)

  await InboxStatusModel.seen(inbox._id, userId)
  // Return
  return response.r200(res, { inbox })
}

/**
 * Show by user id
 *
 */
const showByUserId = async (req, res) => {
  const currentUserId = req.user._id
  const { userId } = req.params
  const inbox = await InboxModel.getByUserId(currentUserId, userId)
  // Return
  return response.r200(res, { inbox })
}

/**
 * Get inbox messages
 *
 */
const getMessages = async (req, res) => {
  const { page } = req.query
  const limit = config.limit.l20

  const messages = await InboxMessageModel.getByInbox(req.inboxData._id, { page, limit })
  return response.r200(res, { messages, limit })
}

/**
 * Get inbox messages
 *
 */
const sendMessage = async (req, res) => {
  const userId = req.user._id
  const inboxId = req.inboxData._id
  const { content } = req.body

  if (!content) {
    return response.r400(res, config.message.contentRequired)
  }

  const message = await InboxMessageModel.newMessage(inboxId, userId, content)
  return response.r200(res, { message })
}

export default {
  all,
  show,
  showByUserId,
  getMessages,
  sendMessage,
}

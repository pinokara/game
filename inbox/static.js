import lodash from 'lodash'
import { to } from '../../utils'
import { InboxModel, InboxStatusModel } from '../model'

/**
 * Get list users in inbox, except current user
 */
const getUsersInInbox = async (inboxId, userId) => {
  const { data } = await to(InboxModel.findOne({ _id: inboxId }).populate('users', 'username').lean())
  if (!data || !data.users || !data.users.length) return []

  const { users } = data
  if (userId) {
    lodash.remove(users, item => item._id.toString() === userId.toString())
  }
  return users
}

/**
 * Detail info
 *
 */
const detailInfo = async (data, userId) => {
  const result = await Promise.all([{
    users: await getUsersInInbox(data._id, userId),
  }])

  return Object.assign(
    lodash.pick(data, ['_id']),
    result[0],
  )
}

/**
 * Get inbox by user id
 *
 */
const getByUserId = async (currentUserId, targetUserId) => {
  const { data } = await to(InboxModel.findOne({
    $or: [{
      users: [currentUserId, targetUserId],
    }, {
      users: [targetUserId, currentUserId],
    }],
  }).lean())

  if (data) {
    const result = await detailInfo(data, currentUserId)
    return result
  }

  // Create new if not found
  const inbox = new InboxModel({
    users: [currentUserId, targetUserId],
  })

  await inbox.save()

  // Create status
  const currentUserStatus = new InboxStatusModel({ user: currentUserId, inbox: inbox._id })
  const targetUserStatus = new InboxStatusModel({ user: targetUserId, inbox: inbox._id })
  await currentUserStatus.save()
  await targetUserStatus.save()

  const result = await detailInfo(inbox.toJSON())
  return result
}

export default {
  getUsersInInbox,
  detailInfo,
  getByUserId,
}

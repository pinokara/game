import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  inbox: {
    type: Schema.Types.ObjectId,
    ref: 'Inbox',
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  content: String,
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

schema.index({ inbox: 1 }).index({ user: 1 })

schema.statics = statics

export default mongoose.model('InboxMessage', schema)

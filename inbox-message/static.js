import lodash from 'lodash'
import { to } from '../../utils'
import { InboxMessageModel, InboxStatusModel } from '../model'

/**
 * Get inbox last message
 *
 */
const getLastMessage = async (inboxId) => {
  const { data } = await to(InboxMessageModel.findOne({ inbox: inboxId }).sort('-createdAt').populate('user', 'username').lean())
  if (!data) return null
  return lodash.pick(data, ['_id', 'user', 'content', 'createdAt'])
}

/**
 * Get inbox messages
 *
 */
const getByInbox = async (inboxId, { page = 0, limit }) => {
  const { data } = await to(InboxMessageModel.find({ inbox: inboxId })
    .sort('-createdAt').populate('user', 'username')
    .skip(page * limit).limit(limit).lean())
  const result = await Promise.all(data.map((item) => {
    return lodash.pick(item, ['_id', 'user', 'content', 'createdAt'])
  }))
  return result
}

/**
 * Send new message
 *
 */
const newMessage = async (inboxId, userId, content) => {
  const message = new InboxMessageModel({
    inbox: inboxId,
    user: userId,
    content,
  })

  await message.save()

  // Update status
  await InboxStatusModel.updateStatusOfSender(inboxId, userId)
  await InboxStatusModel.updateStatusOfReceiver(inboxId, userId)
}

export default {
  getLastMessage,
  getByInbox,
  newMessage,
}

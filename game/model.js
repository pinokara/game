import { mongoose, Schema } from '../../utils/mongoose'
import statics from './static'

const GameSchema = new Schema({
  gameId: String,
  options: Schema.Types.Mixed,
  isActive: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

/**
 * Static functions
 *
 */
GameSchema.statics = statics

// Export
export default mongoose.model('Game', GameSchema)

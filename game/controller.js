import moment from 'moment'
import config from '../../configs'
import gameplayConfig from '../../configs/gameplay'
import { SlotMachine5x3, SlotMachine3x3 } from '../../gameplay/slot'
import LuckyWheel from '../../gameplay/lucky-wheel'
import { response, helper } from '../../utils'
import { UserModel, GameHistoryModel, LuckyWheelHistoryModel, GameConfigModel } from '../../packages/model'

/**
 * Common function for play slot game
 */
const playSlot = async (req, res, ClassType, { gameType }) => {
    // const userId ="5e17647841c87b7d239210f5"
    const userId = req.user._id
    const { lines, betValue, cheatItems, isTrial } = req.body
    let user = await UserModel.fullById(userId)

    // console.log("Slot lines: " + lines);
    // console.log("Slot betValue: " + betValue);
    // console.log("Slot cheatItems: " + cheatItems);

    let freeSpin = await helper.getFreeSpin(gameType, betValue, userId)
    console.log("userId ", userId, "freespin", freeSpin);
    // If admin, return
    if (user.role === config.user.roles.admin) {
        return response.r400(res, config.message.adminCannotPlayGame)
    }
    if (!isTrial && freeSpin <= 0) {
        const totalBet = parseFloat(betValue) * lines.length
        if (totalBet <= 0) {
            return response.r400(res, config.message.betValueInvalid)
        }
        const enoughGemToPlay = await UserModel.checkEnoughGem(user, totalBet)
        // Check enough gem
        console.log("totalBet ", totalBet)
        console.log("enoughGemToPlay ", enoughGemToPlay)
        if (!enoughGemToPlay) {
            return response.r400(res, config.message.notEnoughGemToPlay)
        }
    }
    // if (!user.isBot) {
    //   console.log("update coin "+ user.username +" totalbet: -"+ totalBet)
    //   await UserModel.updateDocById(user._id, { $inc: { gem: (totalBet * -1) } })
    // }

    // Check game banned
    const isBanned = UserModel.checkBannedGame(user, gameType)
    if (isBanned) {
        return response.r400(res, config.message.youBannedFromGame)
    }

    // Start game step by step
    const game = new ClassType(gameType, userId, betValue, lines, cheatItems, isTrial)
    if (!game.isValidBetValue()) {
        return response.r400(res, config.message.betValueInvalid)
    }
    await game.loadGameConfig()
    if (!isTrial) {
        await game.createHistory(freeSpin)
    }
    await game.generateResult()
    if (freeSpin > 0) {
        await helper.increaseFreeSpinSlot(gameType, userId, betValue, -1)
    }

    // await game.generateMatrix()
    // await game.checkMatch()
    await game.updateHistory()
    // get freeSpin
    freeSpin = await helper.getFreeSpin(gameType, betValue, userId)
    user = await UserModel.fullById(userId)

    // Return
    return response.r200(res, {
        spinResult: game.spinResult,
        winLines: game.win,
        winGem: game.winGem,
        userGem: user.gem,
        isWinJackpot: game.isWinJackpot,
        bonus: game.bonus,
        bonusJackpot: game.bonusJackpot,
        vpoint: game.vpoint,
        currentJackpotValue: game.currentJackpotValue ? game.currentJackpotValue : 0,
        freeSpin: freeSpin,
        bonusItemsReward: game.bonusItemsReward
    })
}

const slotTamQuoc = async (req, res) => {
    const cfg = gameplayConfig.get()
    const options = {
        gameType: cfg.games.slotTamQuoc,
    }
    await playSlot(req, res, SlotMachine5x3, options)
}

const slotLienQuan = async (req, res) => {
    const cfg = gameplayConfig.get()
    const options = {
        gameType: cfg.games.slotLienQuan,
    }
    await playSlot(req, res, SlotMachine5x3, options)
}

const slotDC = async (req, res) => {
    const cfg = gameplayConfig.get()
    const options = {
        gameType: cfg.games.slotDC,
    }
    await playSlot(req, res, SlotMachine5x3, options)
}

const slotPUBG = async (req, res) => {
    const cfg = gameplayConfig.get()
    const options = {
        gameType: cfg.games.slotPUBG,
    }
    await playSlot(req, res, SlotMachine5x3, options)
}


const luckyWheel = async (req, res) => {
    const game = new LuckyWheel(req.user._id)
    let error = ''

    await game.loadGameConfig()

    error = await game.getUserData()
    if (error) {
        return response.r400(res, error)
    }

    error = await game.checkAvailableSpin()
    if (error) {
        return response.r400(res, error)
    }

    error = await game.spin()
    if (error) {
        return response.r400(res, error)
    }

    const user = await UserModel.fullById(req.user._id)

    return response.r200(res, {
        prize: game.prize,
        userGem: user.gem,
        wheelSpinTimes: user.wheelSpinTimes,
    })
}

/**
 * Get game ranking
 *
 */
const ranking = async (req, res) => {
    const { gameType } = req.params

    const result = await UserModel.getRankingByGameType(gameType)
    return response.r200(res, { list: result })
}

/**
 * Recent big wins of big small
 *
 */
const bigWin = async (req, res) => {
    const { gameType } = req.params
    const result = await GameHistoryModel.bigWin(gameType)
    console.log('gameType ', gameType);
    console.log('bigWin result ', result);
    return response.r200(res, { list: result })
}
/**
 * Recent big wins of big small
 *
 */
const rankingWinLose = async (req, res) => {
    const { gameType } = req.params
    const result = await GameHistoryModel.rankingWinLose(gameType)
    console.log('gameType ', gameType);
    console.log('bigWin result ', result);
    return response.r200(res, { list: result })
}

const luckyWheelShow = async (req, res) => {
    const gameConfig = await GameConfigModel.findOneByCondition({ gameType: 'luckyWheel' })
    if (!gameConfig.options || !gameConfig.options.active) {
        return response.r404(res, config.message.luckyWheel.notAvailable)
    }
    return response.r200(res, { luckyWheel: gameConfig.options })
}

const luckyWheelHistories = async (req, res) => {
    const startAt = new Date()
    const endAt = new Date(moment().endOf('d').toISOString())
    const result = await LuckyWheelHistoryModel.findByCondition({ startAt, endAt }, { page: 0, limit: config.limit.l20 })
    return response.r200(res, { list: result })
}

const slotInfo = async (req, res) => {
    const { gameType, betValue } = req.params
    // const cfg = gameplayConfig.get()
    // const options = {
    //     gameType: cfg.games.slotTamQuoc,
    // }
    const userId = req.user._id
    let freeSpin = await helper.getFreeSpin(gameType, betValue, userId)
    console.log("userId ", userId, "freespin", freeSpin);
    return response.r200(res, { userId: userId, freeSpin: freeSpin })
}

export default {
    slotLienQuan,
    slotTamQuoc,
    slotDC,
    luckyWheel,
    ranking,
    bigWin,
    luckyWheelShow,
    luckyWheelHistories,
    rankingWinLose,
    slotPUBG,
    slotInfo,
}

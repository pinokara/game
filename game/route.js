/**
 * Game routes
 * prefix: /games
 */

import express from 'express'
import middleware from '../system/middleware'
import GameCtrl from './controller'
import validation from './validation'

const router = express.Router()

/**
 * @apiDefine Header
 * @apiHeader {string} Authorization Access token
 */

/**
 * @api {post} /games/slotTamQuoc slotTamQuoc
 * @apiUse Header
 * @apiGroup Game
 * @apiName slotTamQuoc
 *
 * @apiParam {array[integer]} lines
 * @apiParam {number{0..}} betValue
 * @apiParam {array[integer]} cheatItems
 */
router.post('/slotTamQuoc', middleware.requiresLogin, validation.slot5x3, GameCtrl.slotTamQuoc)

/**
 * @api {post} /games/slotLienQuan slotLienQuan
 * @apiUse Header
 * @apiGroup Game
 * @apiName slotLienQuan
 *
 * @apiParam {array[integer]} lines
 * @apiParam {number{0..}} betValue
 * @apiParam {array[integer]} cheatItems
 */
router.post('/slotLienQuan', middleware.requiresLogin, validation.slot5x3, GameCtrl.slotLienQuan)

/**
 * @api {post} /games/slotPUBG slotPUBG
 * @apiUse Header
 * @apiGroup Game
 * @apiName slotPUBG
 *
 * @apiParam {array[integer]} lines
 * @apiParam {number{0..}} betValue
 * @apiParam {array[integer]} cheatItems
 */
router.post('/slotPUBG', middleware.requiresLogin, validation.slot5x3, GameCtrl.slotPUBG)


/**
 * @api {post} /games/slotDC slotDC
 * @apiUse Header
 * @apiGroup Game
 * @apiName slotDC
 *
 * @apiParam {array[integer]} lines
 * @apiParam {number{0..}} betValue
 * @apiParam {array[integer]} cheatItems
 */
router.post('/slotDC', middleware.requiresLogin, validation.slot5x3, GameCtrl.slotDC)

/**
 * @api {post} /games/luckyWheel LuckyWheel
 * @apiUse Header
 * @apiGroup Game
 * @apiName LuckyWheel
 *
 */
router.post('/luckyWheel', middleware.requiresLogin, GameCtrl.luckyWheel)

/**
 * @api {get} /games/:gameType/ranking Game ranking
 * @apiUse Header
 * @apiGroup Game
 * @apiName GameRanking
 * @apiDescription
 * gameType=bigSmall, slotLienQuan, slotTamQuoc, slotDC, slotPUBG
 */
router.get('/:gameType/ranking', middleware.requiresLogin, GameCtrl.ranking)

/**
 * @api {get} /games/:gameType/big-win Big win
 * @apiUse Header
 * @apiGroup Game
 * @apiName Big small big win
 *
 * @apiDescription
 * gameType=bigSmall, slotLienQuan, slotTamQuoc, slotDC, slotPUBG
 */
router.get('/:gameType/big-win', middleware.requiresLogin, GameCtrl.bigWin)

/**
 * @api {get} /games/:gameType/rankingWinLose Big win
 * @apiUse Header
 * @apiGroup Game
 * @apiName Big small rankingWinLose
 *
 * @apiDescription
 * gameType=bigSmall, slotLienQuan, slotTamQuoc, slotDC, slotPUBG
 */
router.get('/:gameType/rankingWinLose', middleware.requiresLogin, GameCtrl.rankingWinLose)

/**
 * @api {get} /games/luckyWheel Lucky wheel show
 * @apiUse Header
 * @apiGroup Game
 * @apiName Lucky wheel show
 */
router.get('/luckyWheel', middleware.requiresLogin, GameCtrl.luckyWheelShow)

/**
 * @api {get} /games/luckyWheel/histories Lucky wheel histories
 * @apiUse Header
 * @apiGroup Game
 * @apiName Lucky wheel histories
 */
router.get('/luckyWheel/histories', middleware.requiresLogin, GameCtrl.luckyWheelHistories)

/**
 * @api {get} /games/slot/:gameType/:betValue 
 * @apiUse Header
 * @apiGroup Game
 * @apiName Get game slot info
 *
 * @apiDescription
 * gameType=slotLienQuan, slotTamQuoc, slotDC, slotPUBG
 */
router.get('/slot/:gameType/:betValue', middleware.requiresLogin, GameCtrl.slotInfo)

export default router

import Joi from 'joi'
import { validateClientData } from '../../utils'
import configs from '../../configs'

const { message } = configs

const obj = {
  betValue: Joi.number().required().options({
    language: {
      key: '{{!betValue}}',
      number: {
        base: `!!${message.betValueMustbeANumber}`,
      },
      any: {
        required: `!!${message.betValueRequired}`,
        empty: `!!${message.betValueRequired}`,
      },
    },
  }),
  lines: Joi.array().items().required().options({
    language: {
      key: '{{!lines}}',
      array: {
        base: `!!${message.linesMustBeAnArray}`,
      },
      any: {
        required: `!!${message.betValueRequired}`,
        empty: `!!${message.betValueRequired}`,
      },
    },
  }),
}

const slot5x3 = (req, res, next) => {
  validateClientData(req, res, next, Joi.object().keys(obj))
}

const slot3x3 = (req, res, next) => {
  validateClientData(req, res, next, Joi.object().keys(obj))
}


export default {
  slot5x3,
  slot3x3,
}

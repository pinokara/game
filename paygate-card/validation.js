import lodash from 'lodash'
import Joi from 'joi'
import { validateClientData } from '../../utils'
import configs from '../../configs'

const { message } = configs

const cardObj = {
  serial: Joi.string().required().options({
    language: {
      key: '{{!serial}}',
      number: {
        base: `!!${message.mobileCard.serialRequired}`,
      },
      any: {
        required: `!!${message.mobileCard.serialRequired}`,
        empty: `!!${message.mobileCard.serialRequired}`,
      },
    },
  }),
  code: Joi.string().required().options({
    language: {
      key: '{{!code}}',
      number: {
        base: `!!${message.mobileCard.cardIdRequired}`,
      },
      any: {
        required: `!!${message.mobileCard.cardIdRequired}`,
        empty: `!!${message.mobileCard.cardIdRequired}`,
      },
    },
  }),
  telco: Joi.string().valid(configs.mobileCard.telco).required().options({
    language: {
      key: '{{!telco}}',
      number: {
        base: `!!${message.mobileCard.networkRequired}`,
      },
      any: {
        required: `!!${message.mobileCard.networkRequired}`,
        empty: `!!${message.mobileCard.networkRequired}`,
        allowOnly: `!!${message.mobileCard.networkNotSupported}`,

      },
    },
  }),

  amount: Joi.number().valid(configs.mobileCard.values).required().options({
    language: {
      key: '{{!amount}}',
      number: {
        base: `!!${message.mobileCard.invalidValue}`,
      },
      any: {
        required: `!!${message.mobileCard.invalidValue}`,
        empty: `!!${message.mobileCard.invalidValue}`,
        allowOnly: `!!${message.mobileCard.invalidValue}`,
      },
    },
  }),
}




const card = (req, res, next) => {
  validateClientData(req, res, next, Joi.object().keys(cardObj))
}

export default {
  card,

}

/* eslint-disable linebreak-style */
/**
 * Find condition from object
 *
 * @param {Object} condition allow _id properties
 */
const findByCondition = ({ status, start, end, network }) => {
  const condition = {}

  if (status === 'charged') {
    condition.isCharged = true
  } else if (status === 'uncharged') {
    condition.isCharged = false
  }

  if (start && end) {
    condition.createdAt = {
      $gte: start,
      $lte: end,
    }
  }

  if (network) {
    condition.network = network
  }

  return condition
}

export default {
  findByCondition,
}

import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  user_id: String,
  username: String,
  mrc_order_id: String,
  serial: String,
  code: String, // Card network: Mobi, Vina, ...
  amount: Number, // Card value: 10k, 20k, ...
  tmp_amount: Number,
  telco: String,
  order_id: String,
  status: {
    type: Number,
    default: 0,
  },
  chargedAt: Date,
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

schema.index({ sendBy: 1 }).index({ createdAt: 1 }).index({ isCharged: 1 })

schema.statics = statics

export default mongoose.model('CardTrasaction', schema)

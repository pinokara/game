/**
 * Game routes
 * prefix: /games
 */

import express from 'express'
import middleware from '../system/middleware'
import PayGateCardCtl from './controller'
import validation from './validation'

const router = express.Router()


router.post('/transaction', middleware.requiresLogin, validation.card, PayGateCardCtl.transaction)
router.post('/callback', PayGateCardCtl.callback)


export default router

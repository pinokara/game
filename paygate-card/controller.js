// import lodash from 'lodash'
import axios from 'axios'
import HttpStatus from 'http-status-codes'
import config from '../../configs'
import {response} from '../../utils'
import crypto from 'crypto'
import {UserModel, CardTransactionModel} from '../../packages/model'
import Pusher from 'pusher'

const TokenCard = process.env.TOKEN_API_CARD
const codeRequest = process.env.CODE_REQUEST_CARD
const signature = crypto.createHash('md5').update(TokenCard + codeRequest, 'utf8').digest("hex");

const pusher = new Pusher({
    appId: process.env.PUSHER_APP_ID,
    key: process.env.PUSHER_APP_KEY,
    secret: process.env.PUSHER_APP_SECRET,
    cluster: process.env.PUSHER_APP_CLUSTER
});

const transaction = async (req, res) => {
    let user = await UserModel.getById(req.user._id)

    if (!user || !user._id) return response.r404(res, config.message.userNotFound)

    if (req.body.command !== 'cards') {
        return response.r400(res, config.message.mobileCard.networkNotSupported)
    }
    let transactionId = req.body.code + new Date().getTime();
    let ret = await axios.post('http://45.124.94.225/apis/card/add', {
        client_token_api: TokenCard,
        client_code_request: codeRequest,
        client_signature: signature,
        client_type_card: req.body.telco,
        client_code_card: req.body.code,
        client_seri_card: req.body.serial,
        client_value_card: req.body.amount,
        client_transaction_id: transactionId
    }).catch((err)=>{
        console.log(err)
        return response.r400(res, err.response)
    });
    console.log('Reponse Card', ret);
    ret = ret.data;
    
    if (ret.code !== 999) {
        return response.r400(res, ret.message)
    }

    const doc = new CardTransactionModel({
        user_id: user._id,
        username: user.username,
        mrc_order_id: transactionId,
        serial: req.body.serial,
        code: req.body.code,
        telco: req.body.telco,
        amount: req.body.amount,
        tmp_amount: req.body.amount
    });
    await doc.save();
    return response.r200(res, 'Gửi mã thành công. Chúng tôi đang xử lý vui lòng đợi trong giây lát');
}

const callback = async (req, res) => {
    let transactionId = req.body.card_transaction_id;
    console.log('Transaction ID', transactionId);
    console.log('Amount Real', req.body.card_real_amount);
    console.log('Status', req.body.card_status)

    // get transaction
    let transaction = await CardTransactionModel.findOne({
        mrc_order_id: transactionId,
        status: 0
    });
    if (!transaction || !transaction._id) return res.send(HttpStatus.OK);
    // Check card sucesss
    if (req.body.card_status == 2) {
        console.log('Successs .......')
        const amount = req.body.card_real_amount;
        const gem = amount;
        // update
        await UserModel.updateDocById(transaction.user_id, {$inc: {gem}})
        // update status
        await transaction.updateOne({$set: {status: 1}})

        let user = await UserModel.findById(transaction.user_id);
        console.log('money-event')
        pusher.trigger('money-event', 'money-event-' + user._id, {money: user.gem})
    }
    // Check card fail
    if (req.body.card_status == 3) {
        // update status
        await transaction.update({$set: {status: 2}})
    }
    return res.send(HttpStatus.OK);

}

export default {
    transaction,
    callback
}

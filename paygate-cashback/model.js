import {Schema, mongoose} from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  user_id: String,
  username: String,
  amount: Number,
  account_name: String,
  account_number: String,
  bank_name: String,
  note: String,
  status: {
    type: Number,
    default: 0
  },
  chargedAt: Date,
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

schema.index({sendBy: 1}).index({createdAt: 1}).index({isCharged: 1})

schema.statics = statics

export default mongoose.model('CashBack', schema)

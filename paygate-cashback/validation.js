import lodash from 'lodash'
import Joi from 'joi'
import {validateClientData} from '../../utils'
import configs from '../../configs'

const {message} = configs

const obj = {
    amount: Joi.number().required().options({
        language: {
            key: 'Số tiền ',
            number: {
                base: `không hợp lệ`,
            },
            any: {
                required: `không được trống`,
                empty: `không được trống`,
                allowOnly: `không hợp lệ`,
            },
        },
    }),

    bank_name: Joi.string().required().options({
        language: {
            key: 'Ngân hàng ',
            string: {
                base: 'không hỗ trợ',
            },
            any: {
                required: `không được trống`,
                empty: `không được trống`,
                allowOnly: `không hỗ trợ`,
            },
        },
    }),

    account_name: Joi.string().required().options({
        language: {
            key: 'Tên tài khoản ',
            any: {
                required: `không được trống`,
                empty: `không được trống`,
                allowOnly: `không hợp lệ`,
            },
        },
    }),
    account_number: Joi.string().required().options({
        language: {
            key: 'Số tài khoản ',
            any: {
                required: `không được trống`,
                empty: `không được trống`,
            },
        },
    }),
}

const cashback = (req, res, next) => {
    validateClientData(req, res, next, Joi.object().keys(lodash.pick(obj, ['amount', 'bank_name', 'account_name', 'account_number'])))
}


export default {

    cashback,
}

/**
 * Game routes
 * prefix: /games
 */

import express from 'express'
import middleware from '../system/middleware'
import PayGateCashBackCtl from './controller'
import validation from './validation'

const router = express.Router()


router.post('/cashback', middleware.requiresLogin, validation.cashback, PayGateCashBackCtl.cashback)


export default router

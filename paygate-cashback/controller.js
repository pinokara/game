import config from '../../configs'
import { response } from '../../utils'
import { UserModel, CashBackModel } from '../../packages/model'
import Pusher from 'pusher'


const pusher = new Pusher({
  appId: process.env.PUSHER_APP_ID,
  key: process.env.PUSHER_APP_KEY,
  secret: process.env.PUSHER_APP_SECRET,
  cluster: process.env.PUSHER_APP_CLUSTER,
});

const cashback = async (req, res) => {

  const amount = req.body.amount;
  const bank_name = req.body.bank_name;
  const account_name = req.body.account_name;
  const account_number = req.body.account_number;

  if (amount < 0) {
    return response.r400(res, 'Số tiền không được âm');
  }

  if (amount < 100000) {
    return response.r400(res, 'Số tiền tối thiểu là 100.000');
  }

  let user = req.user;
  if (!user || !user._id) return response.r404(res, config.message.userNotFound)
  var rate = 1.25

  if (amount >= 10000000) {
    rate = 1.23
  }
  // quy đổi tiền
  // const gem = amount * 0.2 + amount;
  const gem = amount * rate;

  if (user.gem < gem) {
    return response.r400(res, 'Bạn không đủ tiền');
  }

  await UserModel.updateDocById(user._id, { $inc: { gem: -1 * gem } });

  // save transaction
  const cashback = new CashBackModel({
    user_id: user._id,
    username: user.username,
    amount: amount,
    account_name: account_name,
    account_number: account_number,
    bank_name: bank_name,
  });
  await cashback.save();
  console.log('request-cashback-event')
  console.log(cashback)
  var message = {
    user_id: user._id,
    payload: cashback
  }
  pusher.trigger('notifications', 'request-cashback-event', message);

  user = await UserModel.getById(user._id);

  return response.r200(res, {
    money: user.gem
  });
};

export default {
  cashback,
}

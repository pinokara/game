/* eslint-disable linebreak-style */
/**
 * Find condition from object
 *
 * @param {Object} condition allow _id properties
 */
const findByCondition = ({ status }) => {
  const condition = {}

  if (status === 'resolved') {
    condition.status = 1
  } else if (status === 'rejected') {
    condition.status = 2
  } else if (status === 'pending') {
    condition.status = 0
  }

  return condition
}

export default {
  findByCondition,
}

import config from '../../configs'
import { response, getError, to, format } from '../../utils'
import { MobileCardExchangeModel, InboxModel, InboxMessageModel } from '../../packages/model'

/**
 * All exchanges
 */
const all = async (req, res) => {
  const limit = config.limit.l20
  const { page = 0, sort } = req.query
  const data = await Promise.all([{
    exchanges: await MobileCardExchangeModel.findByCondition(req.query, { page, limit }, sort),
    total: await MobileCardExchangeModel.countByCondition(req.query),
    limitPerPage: limit,
  }])
  return response.r200(res, data[0])
}

/**
 * Change exchange status
 */
const changeStatus = async (req, res) => {
  const exchange = req.mobileCardExchangeData

  const { error } = await MobileCardExchangeModel.updateById(exchange._id, { $set: { isDone: !exchange.isDone, doneAt: Date.now() } })
  if (error) {
    return response.r400(res, getError.message(error))
  }

  return response.r200(res, {
    isDone: !exchange.isDone,
  })
}

/**
 * Delete exchange
 */
const destroy = async (req, res) => {
  const exchange = req.mobileCardExchangeData

  const { error } = await to(exchange.remove())
  if (error) {
    return response.r400(res, getError.message(error))
  }

  return response.r200(res)
}

/**
 * Send card
 */
const sendCard = async (req, res) => {
  const exchange = req.mobileCardExchangeData
  const { cardId, serial } = req.body

  if (exchange.isDone) {
    return response.r400(res, config.message.mobileCard.exchangeDoneBefore)
  }

  if (!cardId || !serial) {
    return response.r400(res, config.message.mobileCard.mustHaveCardIdAndSerial)
  }

  // Update data
  const { error } = await to(exchange.update({
    cardId,
    serial,
    isDone: true,
  }))

  if (error) {
    return response.r400(res, getError.message(error))
  }

  // Send inbox for user
  const content = `Thẻ ${exchange.network} ${format.number(exchange.value)}đ là ${cardId} - ${serial}`
  const inbox = await InboxModel.getByUserId(global.adminId, exchange.sendBy)
  await InboxMessageModel.newMessage(inbox._id, exchange.sendBy, content)

  return response.r200(res)
}


export default {
  all,
  changeStatus,
  destroy,
  sendCard,
}

/**
 * Find condition from object
 *
 * @param {Object} condition allow _id properties
 */
const findByCondition = ({ status, start, end, network, value }) => {
  const condition = {}

  if (status === 'pending') {
    condition.isDone = false
  } else if (status === 'done') {
    condition.isDone = true
  }

  if (start && end) {
    condition.createdAt = {
      $gte: start,
      $lte: end,
    }
  }

  if (network) {
    condition.network = network
  }

  if (value && value !== '0') {
    condition.value = parseFloat(value)
  }

  return condition
}

export default {
  findByCondition,
}

import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  serial: String,
  cardId: String,
  network: String, // Card network: Mobi, Vina, ...
  value: Number, // Card value: 10k, 20k, ...
  gem: Number,
  sendBy: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  isDone: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

schema.index({ sendBy: 1 }).index({ createdAt: 1 }).index({ isDone: 1 })

schema.statics = statics

export default mongoose.model('MobileCardExchange', schema)

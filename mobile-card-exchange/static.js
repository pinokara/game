import lodash from 'lodash'
import { to } from '../../utils'
import { MobileCardExchangeModel, UserModel } from '../model'
import dbQuery from './query'

/**
 * Find by condition
 */
const findByCondition = async (condition, { page, limit }, sort = '-createdAt') => {
  const query = dbQuery.findByCondition(condition)
  const { data } = await to(MobileCardExchangeModel.find(query).sort(sort).skip(page * limit).limit(limit).lean())
  if (!data || !data.length) return []

  const result = await Promise.all(data.map(async (item) => {
    const obj = await detailInfo(item)
    return obj
  }))
  return result
}

/**
 * Count by condition
 */
const countByCondition = async (condition) => {
  const query = dbQuery.findByCondition(condition)
  const { data } = await to(MobileCardExchangeModel.countDocuments(query))
  return data
}

/**
 * Get detail info of giftcode
 */
const detailInfo = async (doc) => {
  const data = await Promise.all([{
    sendBy: await UserModel.getBriefInfoById(doc.sendBy),
  }])
  const result = data[0]
  result.sendBy = lodash.pick(result.sendBy, ['_id', 'username', 'verified', 'phone'])
  return Object.assign(doc, result)
}

/**
 * Update by id
 */
const updateById = async (_id, payload) => {
  const result = await to(MobileCardExchangeModel.findOneAndUpdate({ _id }, payload))
  return result
}

export default {
  findByCondition,
  countByCondition,
  updateById,
}

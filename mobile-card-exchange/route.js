/**
 * Mobile card exchange routes
 * prefix: /mobile-card-exchange
 */

import express from 'express'
import middleware from '../system/middleware'
import MobileCardExchangeCtrl from './controller'
import { preQuery } from '../../utils'

const router = express.Router()

/**
 * @apiDefine Header
 * @apiHeader {string} Authorization Access token
 */

/**
 * @api {get} /mobile-card-exchange All
 * @apiUse Header
 * @apiGroup MobileCardExchange
 * @apiName All
 *
 * @apiParam {Number} page
 * @apiParam {String} status all, pending, done
 * @apiParam {String} sort -createdAt
 */
router.get('/', middleware.requiresLogin, MobileCardExchangeCtrl.all)

/**
 * @api {patch} /mobile-card-exchange/:exchangeCardId/status Change status
 * @apiUse Header
 * @apiGroup MobileCardExchange
 * @apiName Change status
 *
 */
router.patch('/:exchangeCardId/status', middleware.requiresLogin, MobileCardExchangeCtrl.changeStatus)

/**
 * @api {delete} /mobile-card-exchange/:exchangeCardId Delete
 * @apiUse Header
 * @apiGroup MobileCardExchange
 * @apiName Delete
 *
 */
router.delete('/:exchangeCardId', middleware.requiresLogin, MobileCardExchangeCtrl.destroy)

router.put('/:exchangeCardId/send', middleware.requiresLogin, MobileCardExchangeCtrl.sendCard)

router.param('exchangeCardId', preQuery.mobileCardExchange)

export default router

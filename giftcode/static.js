import moment from 'moment'
import config from '../../configs'
import { to } from '../../utils'
import { GiftCodeModel, UserModel } from '../model'


/**
 * Create new codes
 */
const newDocs = async ({ codes = [], gem, createdBy, role }) => {
  // console.log(role)
  var totalGem = 0;
  const result = await Promise.all(codes.map(async (code) => {

    if (!code) return
    // Check code existed
    const { data } = await to(GiftCodeModel.countDocuments({ code }))
    if (data) {
      return code
    }
    if (role != "admin") {
      await UserModel.updateById(createdBy, { $inc: { gem: -gem } })
    }
    totalGem += parseInt(gem)
    const doc = new GiftCodeModel({ code, gem, createdBy })
    console.log(totalGem)
    await doc.save()
    return
  }))
  // console.log(totalGem)
  return { totalGem: totalGem }
}

/**
 * Redeem code
 */
const redeem = async (userId, code) => {

  // Find code first
  const { data } = await to(GiftCodeModel.findOne({ code }))
  if (!data || !data.active) {
    return { error: config.message.giftcode.notFound }
  }

  if (data.createdBy !== userId) {
    // Only allow user redeem once per day
    const startOfDay = moment().startOf('d').toISOString()
    const countRedeemed = await to(GiftCodeModel.countDocuments({ redeemedBy: userId, redeemedAt: { $gte: startOfDay } }))
    if (countRedeemed.data > 0) {
      return { error: config.message.giftcode.onlyRedeemOncePerDay }
    }
  }

  // Return if used
  if (data.isUsed) {
    return { error: config.message.giftcode.alreadyUsed }
  }

  // Update doc
  data.redeemedBy = userId
  data.redeemedAt = new Date()
  data.isUsed = true
  await data.save()

  // Update user
  await UserModel.updateDocById({ _id: userId }, { $inc: { gem: data.gem || 0 } })
  const user = await UserModel.getById(userId)
  return {
    error: null,
    data: {
      giftcodeGem: data.gem,
      userGem: user.gem,
    },
  }
}

export default {
  redeem,
  newDocs
}

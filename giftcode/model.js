import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  code: String,
  gem: {
    type: Number,
    default: 0,
  },
  isUsed: {
    type: Boolean,
    default: false,
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  redeemedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  redeemedAt: Date,
  createdAt: {
    type: Date,
    default: Date.now,
  },
  active: {
    type: Boolean,
    default: true,
  },
}, {
  versionKey: false,
})

schema.index({ redeemedBy: 1 }).index({ isUsed: 1 }).index({ code: 1 })

schema.statics = statics

export default mongoose.model('GiftCode', schema)

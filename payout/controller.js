import { response, getError } from '../../utils'
import { PayoutModel, UserModel } from '../../packages/model'
import utils from './utils'
import lodash from 'lodash'
import moment from 'moment'
import requestHTTP from 'request'
import config from '../../configs'
import { format, constant } from '../../utils';
import Pusher from 'pusher'

const { REQUEST, PROCESSING, FAILED, SUCCESS } = constant
const MERCHANT_NO = process.env.THEPAY_MERCHANT_NO
const NOTIFYURL = process.env.THEPAY_NOTIFYURL
const APPSECRET = process.env.THEPAY_APPSECRET
const APIURL = process.env.THEPAY_APIURL
const CONTENT_TYPE = 'application/json';
const ACCEPT = 'application/json';
const REVERSE = process.env.THEPAY_REVERSEURL

const pusher = new Pusher({
  appId: process.env.PUSHER_APP_ID,
  key: process.env.PUSHER_APP_KEY,
  secret: process.env.PUSHER_APP_SECRET,
  cluster: process.env.PUSHER_APP_CLUSTER,
});

const createOrder = async (req, res) => {
  let user = req.user
  let data = req.body
  let thePay = {
    merchantNo: MERCHANT_NO,
    name: data.name,
    userNo: user._id,
    bankName: data.bankName,
    bankAccount: data.bankAccount,
    bankBranch: data.bankBranch,
    amount: data.amount,
    mobile: data.mobile,
    datetime: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
    memo: data.memo,
    time: Math.floor(Date.now() / 1000),
    extra: data.extra,
    createDate: new Date()
  }
  const result = await PayoutModel.newDoc(thePay)
  let sign = utils.encryptData(result)
  console.log('Parsse sign:', sign);
  result.sign = sign
  await PayoutModel.updateDocById(result._id, result);

  result.orderNo = result._id.toString()
  result.dateTime = moment(new Date(result.time)).format('YYYY-MM-DD HH:mm:ss')
  result.notifyUrl = NOTIFYURL
  result.reverseUrl = REVERSE
  result.appSecret = APPSECRET

  let requestData = lodash.pick(result, ['merchantNo', 'orderNo', 'amount', 'name', 'bankName', 'bankAccount', 'bankBranch', 'memo', 'mobile', 'datetime', 'notifyUrl', 'reverseUrl', 'extra', 'time', 'appSecret', 'sign'])
  console.log('Request Data', requestData)
  let url = APIURL + '/payout/create'
  requestHTTP.post(url, {
    form: requestData,
    headers: {
      content_type: CONTENT_TYPE,
      accept: ACCEPT
    }
  }, async function (err, httpRes, body) {

    if (err) {
      console.log(err)
      return response.r400(res, config.message.serverError)
    }
    if (body) {
      console.log(body)
      if (body.code == 0) {
        var orderNo = body.orderNo
        var checkPay = await PayoutModel.fullById(orderNo)
        if (checkPay && checkPay.status === REQUEST) {
          await PayoutModel.updateDocById(orderNo, {
            status: SUCCESS,
            tradeNo: body.tradeNo
          })
          await UserModel.updateDocById(checkPay.userNo, { $inc: { gem: checkPay.amount } })
          console.log('update money')
          let resData = {
            amount: checkPay.amount,
            gem: user.gem + checkPay.amount
          }
          return response.r200(res, resData)
        } else {
          return response.r400(res, body.message.dataExisted)
        }
      } else {
        return response.r400(res, body.text)
      }
    }
  })
}

const notifyApi = async (req, res) => {
  let body = req.body
  console.log(body)
  if (body.status === 'PAID') {
    let data = lodash.pick(body, ['amount', 'name', 'bankName', 'bankAccount', 'bankBranch', 'memo', 'mobile', 'fee', 'extra'])
    var checkPay = await PayoutModel.fullById(body.orderNo)
    var user = await UserModel.getById(checkPay.userNo)
    if (user.socket_id && user.socket_id.length > 0) {
      var message = {
        user_id: checkPay.userNo,
        payload: {
          ...data,
          money: user.gem
        }
      }
      pusher.trigger('notifications', 'money-event', message);
    }
  }

  return res.status(HttpStatus.OK).send('success')
}

const reveseApi = async (req, res) => {
  let body = req.body
  console.log(body)
}

export default {
  createOrder,
  notifyApi,
  reveseApi
}

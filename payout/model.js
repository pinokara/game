import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'
import { format, constant } from '../../utils'

const { REQUEST, PROCESSING, FAILED, SUCCESS } = constant

const schema = new Schema({
  merchantNo: {
    type: String
  },
  name: {
    type: String
  },
  userNo: String,
  bankName: {
    type: String,
  },
  bankAccount: {
    type: String
  },
  bankBranch: {
    type: String
  },
  amount: {
    type: String
  },
  memo: {
    type: String
  },
  extra: {
    type: String,
    default: ""
  },
  mobile: String,
  datetime: String,
  tradeNo: String,
  time: Number,
  sign: String,
  status: {
    type: String,
    enum: [REQUEST, PROCESSING, FAILED, SUCCESS],
    default: REQUEST
  },
  createDate: {
    type: Date,
  },
  updateDate: {
    type: Date,
    default: Date.now
  }
})

/**
 * Static functions
 *
 */
schema.statics = statics

export default mongoose.model('payouts', schema)

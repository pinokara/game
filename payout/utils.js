import crypto from 'crypto'

const NOTIFYURL = process.env.THEPAY_NOTIFYURL
const REVERSE = process.env.THEPAY_REVERSEURL
const KEY = process.env.THEPAY_KEY
const encryptData = (data) => {
  // crypto.createHash('2')
  var sign = 'amount=' + data.amount + '&bankAccount=' + data.bankAccount + '&bankName=' + data.bankName + '&datetime=' + data.datetime + '&extra=' + data.extra + '&merchantNo='
    + data.merchantNo + '&mobile=' + data.mobile + '&name=' + data.name + '&notifyUrl=' + NOTIFYURL + '&orderNo=' + data._id + '&reverseUrl=' + REVERSE + '&time=' + data.time + KEY
  console.log('encrypt data', data, sign)
  var parseHash = crypto.createHash('sha256').update(sign, 'utf8').digest('hex');
  return crypto.createHash('md5').update(parseHash, 'utf8').digest('hex').toLocaleUpperCase();
}

module.exports = {
  encryptData
}

import express from 'express'
import ThePayCtrl from './controller'
import middleware from "../system/middleware";

const router = express.Router()
//middleware.requiresLogin,
router.post("/create", middleware.requiresLogin, ThePayCtrl.createOrder)
router.get("/notify", ThePayCtrl.notifyApi)
router.get("/reverse", ThePayCtrl.reveseApi)

export default router

/**
 * Me routes
 * prefix: /me
 */

import express from 'express'
import middleware from '../system/middleware'
import MeCtrl from './controller'

const router = express.Router()


/**
 * @apiDefine Header
 * @apiHeader {string} Authorization Access token
 */

/**
 * @api {get} /me/game-histories/:gameType Game histories
 * @apiUse Header
 * @apiGroup Me
 * @apiName GameHistories
 * @apiDescription
 * gameType=bigSmall, slotDragonBall, slotTwelveDesignations
 */
router.get('/game-histories/:gameType', middleware.requiresLogin, MeCtrl.gameHistories)

/**
 * @api {get} /me/send-gem-histories Send gem histories
 * @apiUse Header
 * @apiGroup Me
 * @apiName SendGemHistories
 */
router.get('/send-gem-histories', middleware.requiresLogin, MeCtrl.sendGemHistories)

/**
 * @api {put} /me/giftcodes/:code Redeem gift code
 * @apiUse Header
 * @apiGroup Me
 * @apiName Redeem gift code
 */
router.put('/giftcodes/:code', middleware.requiresLogin, MeCtrl.redeemGiftCode)

/**
 * @api {put} /me/confirm-phone Confirm phone number
 * @apiUse Header
 * @apiGroup Me
 * @apiName Confirm phone number
 *
 * @apiParam {string} phone
 * @apiParam {string} code
 */
router.put('/confirm-phone', middleware.requiresLogin, MeCtrl.confirmPhoneNumber)


/**
 * @api {post} /me/sendOTP verify telegram otp
 * @apiUse Header
 * @apiGroup Me
 * @apiName get telegram otp
 *
 */
router.get('/sendOTP', middleware.requiresLogin, MeCtrl.sendOTP)

/**
 * @api {post} /me/verifyOTP verify telegram otp
 * @apiUse Header
 * @apiGroup Me
 * @apiName verify telegram otp
 * @apiParam {string} code
 */
router.post('/verifyOTP', middleware.requiresLogin, MeCtrl.verifyOTP)

/**
 * @api {post} /me/checkPhoneNumber check Phone Number
 * @apiUse Header
 * @apiGroup Me
 * @apiName check Phone Number
 *
 * @apiParam {string} phone
 */
router.post('/checkPhoneNumber', middleware.requiresLogin, MeCtrl.checkPhoneNumber)

/**
 * @api {post} /me/changePhoneNumber change Phone Number
 * @apiUse Header
 * @apiGroup Me
 * @apiName change Phone Number
 *
 * @apiParam {string} phone
 * @apiParam {string} code
 */
router.post('/changePhoneNumber', middleware.requiresLogin, MeCtrl.changePhoneNumber)

router.get('/test', MeCtrl.test)

/**
 * @apiDefine Header
 * @apiHeader {string} Authorization Access token
 */

/**
 * @api {get} /me Me
 * @apiUse Header
 * @apiGroup Me
 * @apiName Me
 */
router.get('/', middleware.requiresLogin, MeCtrl.me)

export default router

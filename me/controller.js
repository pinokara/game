import config from '../../configs'
import { response, getError, format } from '../../utils'
import { UserModel, GameHistoryModel, GemHistoryModel, GiftCodeModel, VerificationCodeModel } from '../../packages/model'
import { TelegramModule } from '../../modules'
import moment from 'moment'

const _ = require('lodash');

const { message } = config

/**
 * Get app data
 *
 */
const me = async (req, res) => {
  const userId = req.user._id
  const result = await UserModel.detailById(userId)
  return response.r200(res, { me: result })
}

/**
 * Get histories by game type
 *
 */
const gameHistories = async (req, res) => {
  const userId = req.user._id
  const { gameType } = req.params

  const result = await GameHistoryModel.getByUserId(userId, gameType)
  let data = _.map(result, r => {
    r.createdAtMs = moment(r.createdAt).toDate().getTime()
    return r
  })
  // result.createdAtMs = moment(result.createdAt).unix()
  // console.log("gameHistory: ", result)
  return response.r200(res, { histories: data })
}

/**
 * Get send gem histories
 *
 */
const sendGemHistories = async (req, res) => {
  const userId = req.user._id
  const { page = 0 } = req.query

  const result = await Promise.all([{
    histories: await GemHistoryModel.getUserHistories(userId, page, config.limit.l20),
    total: await GemHistoryModel.totalUserHistories(userId),
  }])

  return response.r200(res, result[0])
}

/**
 * Redeem gift code
 */
const redeemGiftCode = async (req, res) => {
  const userId = req.user._id
  const { code } = req.params

  const { error, data } = await GiftCodeModel.redeem(userId, code)
  if (error) {
    return response.r400(res, error)
  }
  return response.r200(res, data)
}

/**
 * Confirm phone number
 */
const confirmPhoneNumber = async (req, res) => {
  const userId = req.user._id
  req.body.phone = format.phone(req.body.phone)
  const { phone, code } = req.body

  // Check verification code
  const isVerificationCodeValid = await VerificationCodeModel.checkCodeValid(phone, code)
  if (!isVerificationCodeValid) {
    return response.r400(res, message.invalidOTPCode)
  }

  // Check phone exists
  const isPhoneExisted = await UserModel.checkPhoneExisted(phone)
  if (isPhoneExisted) {
    return response.r400(res, message.phoneExisted)
  }

  const { error } = await UserModel.updateDocById(userId, { $set: { phone, verified: true } })
  if (error) {
    return response.r400(res, getError.message(error))
  }
  return response.r200(res)
}

// const updateTelegram = async (req, res) => {
//   const currentUserId = req.user._id
//   const {  telrgram_id } = _.pick(req.body,
//     [ 'telrgram_id']);
//   await UserModel.updateTelegram(currentUserId, telrgram_id);
//   return response.r200(res)
// }

const sendOTP = async (req, res) => {
  const userId = req.user._id
  // const { code } = _.pick(req.body,
  //   ['code']);
  const result = await UserModel.detailById(userId)
  console.log(result)
  if (result && result.isTelegram) {
    await TelegramModule.sendOTP(userId);
    return response.r200(res)
  } else {
    return response.r400(res, 'Tài Khoản chưa liên kết với telegram')
  }
}

const verifyOTP = async (req, res) => {
  const userId = req.user._id
  const { code } = _.pick(req.body,
    ['code']);
  const result = await UserModel.detailById(userId)
  console.log(result)
  const verify = await TelegramModule.verifyOTP(userId, code)
  if (!verify) {
    return response.r200(res)
  } else {
    return response.r400(res, verify)
  }
}

const checkPhoneNumber = async (req, res) => {
  const { phone } = req.body
  // const { code } = _.pick(req.body,
  //   ['code']);
  const isPhoneExisted = await UserModel.checkPhoneExisted(phone)
  if (isPhoneExisted) {
    return response.r400(res, message.phoneExisted)
  }

  return response.r200(res)
}

const changePhoneNumber = async (req, res) => {
  const { phone, code } = req.body
  const isPhoneExisted = await UserModel.checkPhoneExisted(phone)
  if (isPhoneExisted) {
    return response.r400(res, message.phoneExisted)
  }
  console.log(`changePhoneNumber ${code}`)
  const userId = req.user._id
  const result = await UserModel.detailById(userId)
  console.log(result)
  if (result && result.isTelegram) {
    const verify = await TelegramModule.verifyOTP(userId, code)
    console.log(`changePhoneNumber ${code}${verify}`)
    if (!verify) {
      result.phone = phone
      await UserModel.updateDocById(userId, { phone })
      return response.r200(res)
    } else {
      return response.r400(res, verify)
    }
  } else {
    console.log('Tài Khoản chưa liên kết với telegram ')
    return response.r400(res, 'Tài Khoản chưa liên kết với telegram')
  }
}

const test = async (req, res) => {
  // const { id, mingem } = _.pick(req.query,
  //   ['id', 'mingem']);
  // // const data = await UserModel.checkEnoughGemDb(id, mingem)
  // var data = await UserModel.updateDocById(id, { $inc: { gem: mingem } })

  return response.r200(res, { data: "test" })
}

export default {
  me,
  gameHistories,
  sendGemHistories,
  redeemGiftCode,
  confirmPhoneNumber,
  sendOTP,
  checkPhoneNumber,
  changePhoneNumber,
  verifyOTP,
  test,
}

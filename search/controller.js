import lodash from 'lodash'
import { response } from '../../utils'
import { UserModel } from '../../packages/model'

/**
 * Search user
 *
 */
const searchUser = async (req, res) => {
  const { keyword } = req.query
  const user = await UserModel.findOneByCondition({ username: keyword.toLowerCase() })
  if (!user) {
    return response.r200(res, { user: null })
  }

  const result = lodash.pick(user.toJSON(), ['_id', 'username'])
  result.exchangeRate = 1
  if (user.info && user.info.exchangeRateBuy) {
    result.exchangeRate = user.info.exchangeRateBuy
  }
  return response.r200(res, { user: result })
}

export default {
  searchUser,
}

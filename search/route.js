/**
 * Search routes
 * prefix: /search
 */

import express from 'express'
import middleware from '../system/middleware'
import SearchCtrl from './controller'

const router = express.Router()

/**
 * @apiDefine Header
 * @apiHeader {string} Authorization Access token
 */

/**
 * @api {get} /search/user User
 * @apiUse Header
 * @apiGroup Search
 * @apiName User
 *
 * @apiParam {string} keyword
 */
router.get('/user', middleware.requiresLogin, SearchCtrl.searchUser)

export default router

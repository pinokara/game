import lodash from 'lodash'
import { to, validation } from '../../utils'
import { ObjectId } from '../../utils/mongoose'
import { MobileCardModel, UserModel } from '../model'
import dbQuery from './query'

/**
 * Find by condition
 */
const findByCondition = async (condition, { page, limit }, sort = '-createdAt') => {
  const query = dbQuery.findByCondition(condition)
  const { data } = await to(MobileCardModel.find(query).sort(sort).skip(page * limit).limit(limit).lean())
  if (!data || !data.length) return []

  const result = await Promise.all(data.map(async (item) => {
    const obj = await detailInfo(item)
    return obj
  }))
  return result
}

/**
 * Count by condition
 */
const countByCondition = async (condition) => {
  const query = dbQuery.findByCondition(condition)
  const { data } = await to(MobileCardModel.countDocuments(query))
  return data
}

/**
 * Get detail info of giftcode
 */
const detailInfo = async (doc) => {
  const data = await Promise.all([{
    sendBy: await UserModel.getBriefInfoById(doc.sendBy),
  }])
  const result = data[0]
  result.sendBy = lodash.pick(result.sendBy, ['_id', 'username', 'verified', 'phone'])
  return Object.assign(doc, result)
}

/**
 * Update by id
 */
const updateById = async (_id, payload) => {
  const result = await to(MobileCardModel.findOneAndUpdate({ _id }, payload))
  return result
}

/**
 * Done card
 */
const doneCard = async (cardId, exchangeRate) => {
  // Return if card id is not valid
  if (!validation.isObjectId(cardId)) return

  cardId = new ObjectId(cardId)

  // Find card
  const { data } = await to(MobileCardModel.findOne({ _id: cardId }))
  if (!data || !data._id || data.isCharged) return

  // Set card to done
  await updateById(cardId, { $set: { isCharged: true, chargedAt: new Date() } })

  // Increase gem for user
  let { gem } = data
  if (!gem) gem = Math.round(data.value * exchangeRate)

  await UserModel.updateDocById(data.sendBy, { $inc: { gem } })
  return
}

export default {
  findByCondition,
  countByCondition,
  updateById,
  doneCard,
}

/**
 * Game routes
 * prefix: /games
 */

import express from 'express'
import multer from 'multer'
import middleware from '../system/middleware'
import MobileCardCtrl from './controller'
import validation from './validation'
import { preQuery, response } from '../../utils'

const router = express.Router()
const upload = multer({ dest: `${process.cwd()}/uploads/` })

const checkFile = (req, res, next) => {
  // Return if not found file
  if (!req.file) {
    return response.r400(res, 'File not found!')
  }
  next()
}

/**
 * @apiDefine Header
 * @apiHeader {string} Authorization Access token
 */

/**
 * @api {get} /mobile-cards/data Data
 * @apiUse Header
 * @apiGroup MobileCard
 * @apiName Data
 *
 */
router.get('/data', middleware.requiresLogin, MobileCardCtrl.cardsData)

/**
 * @api {post} /mobile-cards Submit
 * @apiUse Header
 * @apiGroup MobileCard
 * @apiName Submit
 *
 * @apiParam {string} cardId
 * @apiParam {string} serial
 * @apiParam {string} network
 * @apiParam {number} value
 */
router.post('/', middleware.requiresLogin, validation.create, MobileCardCtrl.create)

/**
 * @api {post} /mobile-cards/exchange Exchange card
 * @apiUse Header
 * @apiGroup MobileCard
 * @apiName Exchange card
 *
 * @apiParam {string} network
 * @apiParam {number} value
 */
router.post('/exchange', middleware.requiresLogin, validation.exchangeCard, MobileCardCtrl.exchangeCard)

/**
 * @api {get} /mobile-cards All
 * @apiUse Header
 * @apiGroup MobileCard
 * @apiName All
 *
 * @apiParam {Number} page
 * @apiParam {String} status all, used, unused
 * @apiParam {String} sort -createdAt, -chargedAt
 */
router.get('/', middleware.requiresLogin, MobileCardCtrl.all)

/**
 * @api {get} /mobile-cards/export-excel Export excel
 * @apiUse Header
 * @apiGroup MobileCard
 * @apiName Export excel
 *
 * @apiParam {Date} start
 * @apiParam {Date} end
 * @apiParam {String} status
 */
router.get('/export-excel', middleware.requiresLogin, MobileCardCtrl.exportExcel)

/**
 * @api {patch} /mobile-cards/:cardId/status Change status
 * @apiUse Header
 * @apiGroup MobileCard
 * @apiName Change status
 *
 */
router.patch('/:cardId/status', middleware.requiresLogin, MobileCardCtrl.changeStatus)

/**
 * @api {delete} /mobile-cards/:cardId Delete
 * @apiUse Header
 * @apiGroup MobileCard
 * @apiName Delete
 *
 */
router.delete('/:cardId', middleware.requiresLogin, MobileCardCtrl.destroy)

router.post('/import-excel', upload.single('file'), checkFile, middleware.requiresLogin, MobileCardCtrl.importExcel)

router.put('/done-cards', middleware.requiresLogin, MobileCardCtrl.doneCards)

router.param('cardId', preQuery.mobileCard)

export default router

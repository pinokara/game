import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  serial: String,
  cardId: String,
  network: String, // Card network: Mobi, Vina, ...
  value: Number, // Card value: 10k, 20k, ...
  gem: {
    type: Number,
    default: 0,
  },
  sendBy: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  isCharged: {
    type: Boolean,
    default: false,
  },
  isPayConfirmed: {
    type: Boolean,
    default: false,
  },
  chargedAt: Date,
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

schema.index({ sendBy: 1 }).index({ createdAt: 1 }).index({ isCharged: 1 })

schema.statics = statics

export default mongoose.model('MobileCard', schema)

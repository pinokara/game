import lodash from 'lodash'
import xlsx from 'node-xlsx'
import { ExportExcelModule } from '../../modules'
import config from '../../configs'
import { to, response, getError, format } from '../../utils'
import { MobileCardModel, UserModel, ConfigModel, MobileCardExchangeModel } from '../../packages/model'

/**
 * List cards data
 */
const cardsData = async (req, res) => {
  const appConfig = await ConfigModel.getConfig()
  return response.r200(res, {
    ...lodash.pick(config.mobileCard, ['networks', 'values']),
    ...lodash.pick(appConfig, ['exchangeCardRate']),
  })
}

/**
 * Submit new card
 */
const create = async (req, res) => {
  const user = await UserModel.getById(req.user._id)
  if (!user || !user._id) return response.r404(res, config.message.userNotFound)

  const networks = config.mobileCard.networks.map(item => item._id)
  if (!networks.includes(req.body.network)) {
    return response.r400(res, config.message.mobileCard.networkNotSupported)
  }

  // Check existed
  const countResult = await to(MobileCardModel.countDocuments(req.body))
  if (countResult.data) {
    return response.r400(res, config.message.mobileCard.cardAlreadySubmitted)
  }

  const appConfig = await ConfigModel.getConfig()

  const doc = new MobileCardModel({
    ...req.body,
    gem: req.body.value * (appConfig.submitCardRate || 0.8),
    sendBy: user._id,
  })

  const { error } = await to(doc.save())
  if (error) {
    return response.r400(res, getError.message(error))
  }
  return response.r200(res)
}

/**
 * Exchange mobile card
 *
 */
const exchangeCard = async (req, res) => {
  let user = await UserModel.fullById(req.user._id)
  if (!user || !user._id) return response.r404(res, config.message.userNotFound)

  const networks = config.mobileCard.networks.map(item => item._id)
  if (!networks.includes(req.body.network)) {
    return response.r400(res, config.message.mobileCard.networkNotSupported)
  }

  const isAgency = config.user.agencies._.split(' ').includes(user.agency)
  if (isAgency) return response.r404(res, config.message.agencyCannotExchangeCard)

  const appConfig = await ConfigModel.getConfig()
  const value = parseFloat(req.body.value)
  const gem = Math.round(value * appConfig.exchangeCardRate)

  // Return if user has not enough gem
  if (user.gem < gem) {
    return response.r400(res, config.message.mobileCard.notEnoughGemToExchange)
  }

  // Save
  const doc = new MobileCardExchangeModel({
    ...req.body,
    gem,
    sendBy: user._id,
  })

  const { error } = await to(doc.save())
  if (error) {
    return response.r400(res, getError.message(error))
  }

  // Update user gem
  await UserModel.updateDocById(user._id, { $inc: { gem: gem * -1 } })
  user = await UserModel.getById(user._id)

  return response.r200(res, {
    userGem: user.gem,
  })
}

/**
 * All cards
 */
const all = async (req, res) => {
  const limit = config.limit.l20
  const { page = 0, sort } = req.query
  const data = await Promise.all([{
    mobileCards: await MobileCardModel.findByCondition(req.query, { page, limit }, sort),
    total: await MobileCardModel.countByCondition(req.query),
    limitPerPage: limit,
  }])
  return response.r200(res, data[0])
}

/**
 * Export excel
 */
const exportExcel = async (req, res) => {
  const start = new Date(req.query.start)
  const end = new Date(req.query.end)

  if (start.getTime() > end.getTime()) {
    return response.r400(res, 'Thời gian không hợp lệ')
  }

  const data = await Promise.all([{
    mobileCards: await MobileCardModel.findByCondition(req.query, { page: 0, limit: 0 }),
    total: await MobileCardModel.countByCondition(req.query),
  }])

  ExportExcelModule.mobileCard(res, data[0])
}

/**
 * Change mobile card status
 */
const changeStatus = async (req, res) => {
  const card = req.mobileCardData

  const { error } = await MobileCardModel.updateById(card._id, { $set: { isCharged: !card.isCharged, chargedAt: Date.now() } })
  if (error) {
    return response.r400(res, getError.message(error))
  }

  return response.r200(res, {
    isCharged: !card.isCharged,
  })
}

/**
 * Delete card
 */
const destroy = async (req, res) => {
  const card = req.mobileCardData

  const { error } = await to(card.remove())
  if (error) {
    return response.r400(res, getError.message(error))
  }

  return response.r200(res)
}

const importExcel = async (req, res) => {
  const workSheetsFromFile = xlsx.parse(req.file.path)
  const { data } = workSheetsFromFile[0]
  data.splice(0, 1)

  if (!data || !data.length) {
    return response.r200(res)
  }

  const appConfig = await ConfigModel.getConfig()

  /* eslint no-await-in-loop:0 */
  for (const item of data) {
    const phone = format.phone(item[0])
    const cardId = item[1].toString()
    const serial = item[2].toString()
    const network = item[3].toString().toLowerCase()
    const value = parseFloat(item[4])
    const isPayConfirmed = parseInt(item[5], 10)
    const gem = value * (appConfig.submitCardRate || 0.8)

    // Return if invalid network
    if (!['viettel', 'mobifone', 'vinaphone'].includes(network)) {
      return
    }

    // Find user from phone
    const user = await UserModel.findOneByCondition({ phone, verified: true })
    // eslint-disable-next-line no-continue
    if (!user || !user._id) continue

    const doc = new MobileCardModel({
      cardId,
      serial,
      network,
      value,
      gem,
      isPayConfirmed: !!isPayConfirmed,
      sendBy: user._id,
    })

    await doc.save()
  }

  return response.r200(res)
}

const doneCards = async (req, res) => {
  const { cards } = req.body

  const appConfig = await ConfigModel.getConfig()
  const exchangeRate = appConfig.submitCardRate || 0.8

  for (const card of cards) {
    await MobileCardModel.doneCard(card, exchangeRate)
  }

  return response.r200(res)
}

export default {
  all,
  changeStatus,
  destroy,
  exportExcel,
  cardsData,
  create,
  exchangeCard,
  importExcel,
  doneCards,
}

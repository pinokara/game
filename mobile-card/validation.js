import lodash from 'lodash'
import Joi from 'joi'
import { validateClientData } from '../../utils'
import configs from '../../configs'

const { message } = configs

const obj = {
  cardId: Joi.string().required().options({
    language: {
      key: '{{!cardId}}',
      number: {
        base: `!!${message.mobileCard.cardIdRequired}`,
      },
      any: {
        required: `!!${message.mobileCard.cardIdRequired}`,
        empty: `!!${message.mobileCard.cardIdRequired}`,
      },
    },
  }),
  serial: Joi.string().required().options({
    language: {
      key: '{{!serial}}',
      number: {
        base: `!!${message.mobileCard.serialRequired}`,
      },
      any: {
        required: `!!${message.mobileCard.serialRequired}`,
        empty: `!!${message.mobileCard.serialRequired}`,
      },
    },
  }),
  network: Joi.string().required().options({
    language: {
      key: '{{!network}}',
      number: {
        base: `!!${message.mobileCard.networkRequired}`,
      },
      any: {
        required: `!!${message.mobileCard.networkRequired}`,
        empty: `!!${message.mobileCard.networkRequired}`,
      },
    },
  }),
  value: Joi.number().valid(configs.mobileCard.values).required().options({
    language: {
      key: '{{!serial}}',
      number: {
        base: `!!${message.mobileCard.invalidValue}`,
      },
      any: {
        required: `!!${message.mobileCard.invalidValue}`,
        empty: `!!${message.mobileCard.invalidValue}`,
        allowOnly: `!!${message.mobileCard.invalidValue}`,
      },
    },
  }),
}

const create = (req, res, next) => {
  validateClientData(req, res, next, Joi.object().keys(obj))
}

const exchangeCard = (req, res, next) => {
  validateClientData(req, res, next, Joi.object().keys(lodash.pick(obj, ['network', 'value'])))
}

export default {
  create,
  exchangeCard,
}

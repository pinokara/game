import moment from 'moment-timezone'
import { to } from '../../utils'
import { UserAndLuckyWheelStatus, UserModel } from '../model'

const timezone = 'Asia/Ho_Chi_Minh';
const maxSpin = 1

const increaseSpinTimes = async (userId) => {
  const user = await UserModel.fullById(userId)
  const startDate = moment.tz(new Date(), timezone).startOf('D').toISOString(true)
  const endDate = moment.tz(new Date(), timezone).endOf('D').toISOString(true)
  const query = {
    user: userId,
    createdAt: {
      $gte: new Date(startDate),
      $lte: new Date(endDate),
    },
  }

  console.log(query)
  const { data } = await to(UserAndLuckyWheelStatus.countDocuments(query))

  if (data > 0) {
    return user.wheelSpinTimes
  }

  // Save doc
  const doc = new UserAndLuckyWheelStatus({
    user: userId,
    spin: maxSpin,
  })
  await doc.save()

  await UserModel.updateDocById(userId, { wheelSpinTimes: maxSpin })
  return maxSpin
}

export default {
  increaseSpinTimes,
}

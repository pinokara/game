import { mongoose, Schema } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
  },
  spin: {
    type: Number,
    default: 0,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

// Index
schema.index({ user: 1 })
schema.statics = statics

// Export
export default mongoose.model('UserAndLuckyWheelStatus', schema)

import crypto from 'crypto'
import uniqueValidator from 'mongoose-unique-validator'
import increment from 'mongoose-increment'
import { Schema, mongoose } from '../../utils/mongoose'
import configs from '../../configs'
import statics from './static'
import { format, constant } from '../../utils'

const { REQUEST, PROCESSING, FAILED, SUCCESS } = constant

const { message } = configs

const RequestURL = process.env.REQUEST_URL
const ReturnURL = process.env.RETURN_URL
const schema = new Schema({
  customerId: {
    type: String
  },
  username: {
    type: String
  },
  transTime: {
    type: String
  },
  transId: {
    type: String,
  },
  checkSum: {
    type: String
  },
  curr: {
    type: String
  },
  amount: {
    type: Number
  },
  custIP: {
    type: String
  },
  bankCode: {
    type: String
  },
  status: {
    type: String,
    enum: [REQUEST, PROCESSING, FAILED, SUCCESS],
    default: REQUEST
  },
  createDate: {
    type: Date,
  },
  updateDate: {
    type: Date,
    default: Date.now
  }


})

/**
 * Static functions
 *
 */
schema.statics = statics

// schema.plugin(increment, {
//   type: String,
//   modelName: 'xpay',
//   fieldName: 'refId'
// });

// schema.plugin(uniqueValidator, { message: 'RefID must be unique' })


/**
 * Presave hook
 */
// schema.pre('save', function (next) {
//   var data = 'MerchantID=' + this.merchantId + '&CustID=' + this.customerId + '&Curr=' + this.curr + '&Amount=' + this.amount + '&RefID=' + this.refId + '&TransTime=' + this.transTime + '&ReturnURL=' + ReturnURL + '&RequestURL=' + RequestURL + '&BankCode=' + this.bankCode
//   // XSS name
//   this.checkSum = crypto.createHash('md5').update(data).digest("hex");

// //   this.username = this.username.toLowerCase()

//   next()
// })

export default mongoose.model('xpay', schema)

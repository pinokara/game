import lodash from 'lodash'
import config from '../../configs'
import { XPayModel, ConfigModel } from '../model'
import { to, helper, format, validation } from '../../utils'
import dbQuery from './query'

const newDoc = async (xpay) => {
  const doc = new XPayModel(xpay)
  const result = await doc.save()
  return result
}

const findOneByCondition = async (condition) => {
  const query = dbQuery.findByCondition(condition)
  console.log(query)
  const { data } = await to(XPayModel.findOne(query))
  return data
}

/**
 * Get full data by id
 *
 * @param {ObjectId} _id
 */
const fullById = async (_id) => {
  if (!_id) return {}

  const query = dbQuery.findByCondition({ _id })
  const { data } = await to(XPayModel.findOne(query).lean())
  return data
}

/**
 * Update by id
 *
 * @param {ObjectId}  _id   user id
 * @param {Object}    body  update data
 */
const updateDocById = async (_id, payload) => {
  const result = await to(XPayModel.updateOne({ _id }, payload))
  return result
}

export default {
    newDoc,
    findOneByCondition,
    fullById,
    updateDocById,
  }

const uft8 = require('utf8')
const queryString = require('querystring')

const strSplit = Array("", "g", "h", "G", "k", "g", "J", "K", "I", "h", "i", "j", "H");
let n = 0;
const encrypt = (data) => {
    let strTemp = "";
    if (!data || data == "") {
        return strTemp;
    }
    let text = uft8.encode(data);
    text = text.replace(/\r\n/g, '')
    let buffer = Buffer.from(text)

    let sTemp = buffer.join(",")
    let arr = sTemp.split(",")
    for (let i = 0; i < arr.length; i++) {
        const number = arr[i]
        encryptToInt16(number)
        let encryptData = encryptToInt16(number)
        strTemp += encryptData
    }
    return strTemp
}
const intToHex = (d) => {
    let number = (+d).toString(16).toUpperCase()
    if ((number.length % 2) > 0) { number = "0" + number }
    return number
}
const encryptToInt16 = (number) => {
    if (n === 12) {
        n = 1
    } else {
        n++
    }

    let myChar = "H"
    try {
        myChar = strSplit[n]
    } catch (errr) {
        n = 1
    }

    myChar = strSplit[n]
    // console.log(myChar)
    let hex = intToHex(number)
    // console.log(hex)
    return hex + myChar;
}

const decrypt = (data) => {
    let str = ''
    if (data && data.length > 0) {
        data = data.replace(/\r\n/g, "")
        data = data.replace(/ /g, ",");
        data = data.replace(/g/g, ",");
        data = data.replace(/G/g, ",");
        data = data.replace(/H/g, ",");
        data = data.replace(/h/g, ",");
        data = data.replace(/K/g, ",");
        data = data.replace(/k/g, ",");
        data = data.replace(/J/g, ",");
        data = data.replace(/j/g, ",");
        data = data.replace(/I/g, ",");
        data = data.replace(/i/g, ",");
        data = data.toUpperCase()
        let arr = data.split(",")
        for (let i = 0; i < arr.length; i++) {
            let number = parseInt(arr[i], 16)
            let strDecrypt = String.fromCharCode(number)
            str += strDecrypt
        }
    }
    return queryString.parse(str,null,null,decodeURIComponent(str));
}
export default {
    encrypt,
    decrypt,
}

// var data = "MerchantID=US00001&CustID=22006&Curr=CNY&Amount=1.00&RefID=1714521627&TransTime=2011-04-14 05:21:05&ReturnURL=http://www.payment-quickpay.com/QuickPay_Staging/MerchantReceive.aspx&RequestURL=&BankCode=PGateway"
// var encryptData = encrypt(data)
// console.log("encryptData", encryptData)
// var decryptData = decrypt(encryptData)
// console.log(decryptData)

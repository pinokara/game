import express from 'express'
import XPayCtrl from './controller'
import middleware from "../system/middleware";

const router = express.Router()
//middleware.requiresLogin,
router.post("/requestdeposit", middleware.requiresLogin, XPayCtrl.requestDeposit)
router.get("/recevicecallback", XPayCtrl.receiveCallback)
router.get("/returncalback", XPayCtrl.returnCallback)

export default router

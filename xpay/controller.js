import moment from 'moment'
import currencyFormater from 'currency-formatter'
import config from '../../configs'
import gameplayConfig from '../../configs/gameplay'
import { response, getError, helper } from '../../utils'
import { XPayModel, GemHistoryModel, UserModel } from '../../packages/model'
import crypto from 'crypto'
import utils from './utils'
import utf8 from 'utf8'
import { format, constant } from '../../utils';
import HttpStatus from 'http-status-codes'
import path from 'path'
import Pusher from 'pusher'

const { REQUEST, PROCESSING, FAILED, SUCCESS } = constant

const merchantId = process.env.MERCHANT_ID
const encryptionKeys = process.env.ENCRYPTION_KEYS
const userNameXpay = process.env.USER_NAME_XPAY
const RequestURL = process.env.REQUEST_URL
const ReturnURL = process.env.RETURN_URL


const pusher = new Pusher({
    appId: process.env.PUSHER_APP_ID,
    key: process.env.PUSHER_APP_KEY,
    secret: process.env.PUSHER_APP_SECRET,
    cluster: process.env.PUSHER_APP_CLUSTER,
});

const requestDeposit = async (req, res) => {
    const { bankCode } = req.body

    const amount = parseFloat(req.body.amount)
    if (amount < 100000) {
        return response.r400(res, config.message.bank.invalidAmount)
    }
    const userId = req.user._id
    const username = req.user.username
    const xpay = {
        customerId: userId,
        username: username,
        curr: 'VND',
        transTime: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
        amount: amount,
        custIP: req.connection.remoteAddress,
        bankCode: bankCode,
        createDate: new Date(),
    }
    const result = await XPayModel.newDoc(xpay);
    var checkSum = md5(result);
    result.checkSum = checkSum;

    await XPayModel.updateDocById(result._id, result);

    let paramData = 'MerchantID=' + merchantId + '&CustID=' + userNameXpay + '&Curr=' + result.curr + '&Amount=' + result.amount + '&RefID=' + result._id + '&TransTime=' + result.transTime + '&ReturnURL=' + ReturnURL + '&RequestURL=' + RequestURL + '&BankCode=' + result.bankCode
    console.log("Data:", paramData)
    let encryptionData = utils.encrypt(paramData);
    return response.r200(res, {
        data: encryptionData,
        remark: 'Transaction' + result._id,
        encryptionText: checkSum,
    })
}

const md5 = (xpay) => {
    var data = encryptionKeys + ":" + merchantId + ',' + userNameXpay + "," + xpay.curr + "," + xpay.amount + "," + xpay._id + "," + xpay.transTime + "," + ReturnURL + "," + RequestURL + "," + xpay.bankCode + "," + 'Transaction' + xpay._id
    console.log("EncryptionText", data);
    var checkSumMd5 = crypto.createHash('md5').update(data, 'utf8').digest("hex");
    var checkSum = checkSumMd5.toUpperCase()
    console.log("checkSumMd5", checkSum)
    return checkSum
}

const receiveCallback = async (req, res) => {
    let { Data, Remarks, EncryptText } = req.query
    const decryptData = utils.decrypt(Data)
    // const arrTmp = decryptData.split('&')
    console.log("decryptData: ", decryptData)
    console.log("EncryptText: ", JSON.stringify(EncryptText))
    // console.log("decryptData.EncryptText: ", decryptData.EncryptText.replace(/\s/g,""))
    const checkSum = decryptData.EncryptText.replace('\u0000', "");
    console.log("checkSum: ", JSON.stringify(checkSum))
    if (JSON.stringify(EncryptText) === JSON.stringify(checkSum)) {
        const status = decryptData.Status
        const xTransactionID = utf8.encode(decryptData.TransID)
        const xValidationKey = utf8.encode(decryptData.ValidationKey)
        if (status === '002') {
            const refId = utf8.encode(decryptData.RefID)
            const xpay = await XPayModel.fullById(refId)
            console.log("refId: ", JSON.stringify(xpay))
            if (xpay.status !== FAILED || xpay.status !== SUCCESS) {
                await XPayModel.updateDocById(refId, { status: SUCCESS })

                let amountPayTmp = parseInt(xpay.amount)
                // let mountBounus = 0

                let amountPay = amountPayTmp * 2;
                // switch (amountPayTmp) {
                //     case 100000:
                //         amountPay = 110000
                //         mountBounus = 140000
                //         break;
                //     case 200000:
                //         amountPay = 230000
                //         mountBounus = 270000
                //         break;
                //     case 500000:
                //         amountPay = 580000
                //         mountBounus = 670000
                //         break;
                //     case 1000000:
                //         amountPay = 1175000
                //         mountBounus = 1325000
                //         break;
                //     default:
                //         amountPay = amountPay + amountPay * 0.176;
                //         mountBounus = (amountPay * 1.5) - (amountPay * 0.176)
                // }

                await UserModel.updateDocById(xpay.customerId, { $inc: { gem: amountPay } })
                let user = await UserModel.getById(xpay.customerId)
                console.log("update money")
                pusher.trigger('money-event', 'money-event-' + user._id, { money: user.gem })
                // console.log('Send giftcode 1,5', mountBounus)
                // helper.sendBonusGiftCode(user._id, mountBounus)
            }
        }
        return res.status(HttpStatus.OK).send(xTransactionID + "||" + xValidationKey)
    } else {
        return res.status(HttpStatus.BAD_REQUEST).send("")
    }
}

const returnCallback = async (req, res) => {
    console.log(req.query)
    const { Data, EncryptText } = req.query
    const decryptData = utils.decrypt(Data)
    // // const arrTmp = decryptData.split('&')
    console.log(decryptData)
    console.log("decryptData: ", decryptData)
    console.log("EncryptText: ", JSON.stringify(EncryptText))
    // console.log("decryptData.EncryptText: ", decryptData.EncryptText.replace(/\s/g,""))
    const checkSum = decryptData.EncryptText.replace('\u0000', "");
    console.log("checkSum: ", JSON.stringify(checkSum))
    if (JSON.stringify(EncryptText) === JSON.stringify(checkSum)) {
        const status = decryptData.Status
        const xTransactionID = utf8.encode(decryptData.TransID)
        const xValidationKey = utf8.encode(decryptData.ValidationKey)
        if (status === '002') {
            res.sendFile(path.join(__dirname + '../../../../success.html'))
        } else {
            res.sendFile(path.join(__dirname + '../../../../failed.html'))
        }
    } else {
        res.sendFile(path.join(__dirname + '../../../../failed.html'))
    }
}

export default {
    requestDeposit,
    receiveCallback,
    returnCallback,
}

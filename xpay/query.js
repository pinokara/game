import { ObjectId } from '../../utils/mongoose'
import { validation, format } from '../../utils'

const findByCondition = ({ _id, refId }) => {
  const condition = {}
  if (validation.isObjectId(_id)) {
    condition._id = new ObjectId(_id)
  }

  if (refId) {
    condition.refId = refId
  }

  return condition
}

export default {
  findByCondition,
}

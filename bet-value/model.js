import { Schema, mongoose } from '../../utils/mongoose'
import statics from './static'


const schema = new Schema({
  jackpot: {
    type: Schema.Types.ObjectId,
    ref: 'Jackpot',
  },
  value: {
    type: Number,
  },
  percentAddToJackpot: {
    type: Number,
  },
}, {
  collection: 'bet-values',
  versionKey: false,
})

schema.index({ jackpot: 1 })
schema.statics = statics

export default mongoose.model('BetValue', schema)

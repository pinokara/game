import { response } from '../../utils'
import { GameStatModel } from '../../packages/model'

/**
 * Get game stats
 *
 */
const stats = async (req, res) => {
  const { gameId } = req.params
  const { gameType } = req.query
  const gameStats = await GameStatModel.getByGameId(gameId, gameType)

  // Return
  return response.r200(res, { stats: gameStats })
}

export default {
  stats,
}

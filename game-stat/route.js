/**
 * Game routes
 * prefix: /game-stats
 */

import express from 'express'
import middleware from '../system/middleware'
import GameStatCtrl from './controller'

const router = express.Router()

/**
 * @apiDefine Header
 * @apiHeader {String} Authorization Access token
 */

/**
 * @api {get} /game-stats/:gameId Get game stats
 * @apiUse Header
 * @apiGroup GameStat
 * @apiName Show
 *
 * @apiParam {string=bigSmall} gameType
 */
router.get('/:gameId', middleware.requiresLogin, GameStatCtrl.stats)

export default router

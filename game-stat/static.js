import moment from 'moment'
import { to } from '../../utils'
import { GameStatModel, GameHistoryModel } from '../model'

/**
 * Get game stats by game id
 *
 * @param {String} gameId
 * @param {String} gameType
 */
const getByGameId = async (gameId, gameType) => {
  const now = new Date()
  const daysAgo = new Date(moment().subtract(10, 'd').toISOString())

  const { data } = await to(GameStatModel.findOne({
    gameId: gameId.toString(),
    gameType,
    startAt: {
      $gte: daysAgo,
      $lte: now,
    },
  }).select('gameId gameType stats').lean())

  // Return if game not found
  if (!data) {
    return {
      game: {},
      list: [],
    }
  }

  const list = await GameHistoryModel.getByGameId(gameType, gameId)
  return {
    game: data,
    list,
  }
}

export default {
  getByGameId,
}

import { mongoose, Schema } from '../../utils/mongoose'
import statics from './static'

const schema = new Schema({
  gameId: String,
  gameType: String,
  stats: Schema.Types.Mixed,
  endAt: {
    type: Date,
    default: Date.now,
  },
  startAt: {
    type: Date,
    default: Date.now,
  },
}, {
  versionKey: false,
})

// Index
schema.index({ gameType: 1 }).index({ gameId: 1 }).index({ endAt: 1 })
  .index({ startAt: 1, endAt: 1 })

/**
 * Static functions
 *
 */
schema.statics = statics

// Export
export default mongoose.model('GameStat', schema)
